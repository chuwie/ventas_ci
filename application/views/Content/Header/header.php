<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php base_url(); ?>include/assets/images/favicon.png">
    <title>Admin Panel</title>
    <!-- Custom CSS -->
    <link href="<?php base_url(); ?>include/assets/libs/flot/css/float-chart.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php base_url(); ?>include/dist/css/style.min.css" rel="stylesheet">
    <!-- <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'> -->

    <link href="<?php base_url(); ?>include/dist/css/validationEngine.jquery.css" rel="stylesheet">
    <!-- <link href="<?php echo  base_url(); ?>include/dist/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" > -->
    <!-- <link href="<?php echo  base_url(); ?>include/dist/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" > -->
    <link href="<?php echo  base_url(); ?>include/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url(); ?>include/assets/libs/select2/dist/css/select2.min.css">
    <link href="<?php echo  base_url(); ?>include/assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url(); ?>include/css/custom.css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>