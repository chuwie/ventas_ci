
<div class="main-wrapper">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->

    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Login box.scss -->
    <!-- ============================================================== -->
    <div class="backgroud-img auth-wrapper d-flex no-block justify-content-center align-items-center">
        <div class="auth-box bg-dark border-top border-secondary">
            <div>
                <div class="text-center p-t-20 p-b-20">
                    <!-- <span class="db"><img src="../../assets/images/logo.png" alt="logo" /></span> -->
                    <span class="account">CREA TÚ CUENTA<span>
                </div>
                <!-- Form -->
                <form class="form-register m-t-20">
                    <div class="row p-b-30">
                        <div class="col-12">
                            <div class="row">
                                <span style="color: white;">INFORMACIÓN PERSONAL</span>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="mdi mdi-lead-pencil"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg validate[required]" name="name" id="name" placeholder="Nombre" aria-label="Username" aria-describedby="basic-addon1" >
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="mdi mdi-lead-pencil"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg validate[required]" name="surnames" id="surnames" placeholder="Apellidos" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                                <!-- email -->
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="mdi mdi-email-outline"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg validate[required, custom[email]]" name="email" id="email" placeholder="Email Address" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="mdi mdi-cellphone"></i></span>
                                    </div>
                                    <select class="select-phone" name="code1" id="code1"></select>
                                    <input type="text" class="form-control form-control-lg validate[required, min[8]]" maxlength="9" name="personalphone" id="personalphone" placeholder="Teléfono Personal" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="row">
                                <span style="color: white;">INFORMACIÓN USUARIO</span>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="mdi mdi-account"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg validate[required]" name="username" id="username" placeholder="Nombre de Usuario" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon2"><i class="mdi mdi-lead-pencil"></i></span>
                                    </div>
                                    <input type="password" class="form-control form-control-lg validate[required]" name="password" id="password" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon2"><i class="mdi mdi-lead-pencil"></i></span>
                                    </div>
                                    <input type="password" class="form-control form-control-lg validate[required, equals[password]]" name="confirm" id="confirm" placeholder="Confirm Password" aria-label="Password" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="row">
                                <span style="color: white;">INFORMACIÓN EMPRESA</span>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="mdi mdi-hospital-building"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg validate[required]" name="company" id="company" placeholder="Nombre de la Empresa" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon2"><i class="mdi mdi-car"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg validate[required]" name="direction" id="direction" placeholder="Dirección" aria-label="Password" aria-describedby="basic-addon1">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon2"><i class="mdi mdi-lead-pencil"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg validate[required, min[9]]" maxlength="12" name="rut" id="rut" placeholder="Rut" aria-label="Password" aria-describedby="basic-addon1">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="mdi mdi-cellphone"></i></span>
                                    </div>
                                    <select class="select-phone" name="code2" id="code2"></select>
                                    <input type="text" class="form-control form-control-lg validate[required, min[8]]" maxlength="9" name="phone" id="phone" placeholder="Teléfono Empresa" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row border-top border-secondary">
                        <div class="col-12">
                            <div class="form-group">
                                <div class="p-t-20">
                                    <button class="btn btn-block btn-lg btn-info btn-register" type="button">Registrarse</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Login box.scss -->
    <!-- ============================================================== -->
</div>