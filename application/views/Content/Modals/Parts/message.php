<!-- Inicio Modal Exito -->
<div id="exito" class="modal fade modal-respuestas" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header btn-success">
					<h4 class="modal-title">ÉXITO</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<p class="text-center text-success"><i class="glyphicon glyphicon-ok-circle icons-modals"></i></p>
					<p id="texto_exito" class="p-modals"></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info center-block" data-dismiss="modal" style="max-width: 20%;" id="btn-exito">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
<!-- Fin Modal Exito -->

<!-- Inicio Modal Error -->
	<div id="error" class="modal fade modal-respuestas" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header btn-danger">
					<h4 class="modal-title">Error</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<p class="text-center text-danger"><i class="glyphicon glyphicon-remove-circle icons-modals"></i></p>
					<p id="texto_error" class="p-modals"></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info center-block" data-dismiss="modal" style="max-width: 20%;">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
<!-- Fin Modal Error -->

<!-- Inicio Modal Warning -->
	<div id="alerta" class="modal fade modal-respuestas" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header btn-warning">
					<h4 class="modal-title">Alerta</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<p class="text-center text-warning"><i class="glyphicon glyphicon-warning-sign icons-modals"></i></p>
					<p id="texto_alerta" class="p-modals"></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info center-block" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
<!-- Fin Modal Warning -->