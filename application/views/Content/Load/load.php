<script src="<?php base_url(); ?>include/assets/libs/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?php base_url(); ?>include/assets/libs/popper.js/dist/umd/popper.min.js"></script>
<script src="<?php base_url(); ?>include/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php base_url(); ?>include/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
<script src="<?php base_url(); ?>include/assets/extra-libs/sparkline/sparkline.js"></script>
<!--Wave Effects -->
<script src="<?php base_url(); ?>include/dist/js/waves.js"></script>
<!--Menu sidebar -->
<script src="<?php base_url(); ?>include/dist/js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="<?php base_url(); ?>include/dist/js/custom.min.js"></script>
<script src="<?php base_url(); ?>include/js/custom.js"></script>
<script src="<?php base_url(); ?>include/js/jquery.rut.chileno.js"></script>
<!-- Validation Engine -->
<script src="<?php base_url(); ?>include/dist/js/jquery.validationEngine.js"></script>
<script src="<?php base_url(); ?>include/dist/js/languages/jquery.validationEngine-es.js" type="text/javascript" charset="utf-8"></script>

<script src="<?php base_url(); ?>include/assets/extra-libs/DataTables/datatables.min.js"></script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script> -->

<script src="<?php base_url(); ?>include/assets/libs/select2/dist/js/select2.full.min.js"></script>
<script src="<?php base_url(); ?>include/assets/libs/select2/dist/js/select2.min.js"></script>

<!--This page JavaScript -->
<!-- <script src="<?php base_url(); ?>include/dist/js/pages/dashboards/dashboard1.js"></script> -->
<!-- Charts js Files -->
<script src="<?php base_url(); ?>include/assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
<script src="<?php base_url(); ?>include/dist/js/pages/mask/mask.init.js"></script>
<script src="<?php base_url(); ?>include/assets/libs/flot/excanvas.js"></script>
<script src="<?php base_url(); ?>include/assets/libs/flot/jquery.flot.js"></script>
<script src="<?php base_url(); ?>include/assets/libs/flot/jquery.flot.pie.js"></script>
<script src="<?php base_url(); ?>include/assets/libs/flot/jquery.flot.time.js"></script>
<script src="<?php base_url(); ?>include/assets/libs/flot/jquery.flot.stack.js"></script>
<script src="<?php base_url(); ?>include/assets/libs/flot/jquery.flot.crosshair.js"></script>
<script src="<?php base_url(); ?>include/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="<?php base_url(); ?>include/dist/js/pages/chart/chart-page-init.js"></script>
<!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js'></script> -->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo  base_url(); ?>include/dist/js/jquery.dataTables.min.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo  base_url(); ?>include/dist/js/dataTables.bootstrap.min.js"></script> -->

<script src="<?php echo  base_url(); ?>include/assets/libs/toastr/build/toastr.min.js"></script>
<!-- Funtions -->
