<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">
                <?php if($content == 'dash'){
                    echo 'DASHBOARD';
                }else if($content == 'venta'){
                    echo 'VENTAS';
                }else if($content == 'producto'){
                    echo 'PRODUCTOS';
                }else if($content == 'stock'){
                    echo 'STOCK';
                }else if($content == 'branchoffice'){
                    echo 'SUCURSALES';
                }else if($content == 'categoria'){
                    echo 'CATEGORIA';
                }else if($content == 'marcas'){
                    echo 'MARCAS';
                }else if($content == 'update'){
                    echo 'ACTUALIZAR USUARIOS';
                }
                ?>
            </h4>
            <?php if($user['tipo_user']==0){ 
                        if($content!='categoria' && $content != 'marcas' && $content != 'producto' && $content != 'stock' && $content != 'update'){?>
                <div class="ml-auto text-right">
                    <select class="custom-select ">
                        <?php
                            if(!empty($user['list'])){
                                echo '<option value="">-- Administrador --</option>';
                                foreach($user['list'] as $value){
                                    echo '<option value="'.$value->id.'">'.$value->direccion.'</option>';
                                }
                            }else{
                                echo '<option>-- Sin Sucursales --</option>';
                            }
                        ?>
                        
                    </select>
                </div>
            <?php }
            } ?>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Library</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->