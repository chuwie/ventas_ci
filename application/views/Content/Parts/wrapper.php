<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <?php 
        
        $this->load->view('Content/Parts/breadcrumb');
        if($content == 'dash'){
            $this->load->view('Content/Parts/Container/container_dash');
        }else if($content == 'venta'){
            $this->load->view('Content/Parts/Container/container_ventas');
        }else if($content == 'producto'){
            $this->load->view('Content/Parts/Container/container_producto');
        }else if($content == 'stock'){
            $this->load->view('Content/Parts/Container/container_stock');
        }else if($content == 'branchoffice'){
            $this->load->view('Content/Parts/Container/container_branchoffice');
        }else if($content == 'categoria'){
            $this->load->view('Content/Parts/Container/container_category');
        }else if($content == 'marcas'){
            $this->load->view('Content/Parts/Container/container_brands');
        }else if($content == 'update'){
            $this->load->view('Content/Parts/Container/container_updateuser');
        }
    ?>
</div>
    <?php $this->load->view('Content/Parts/footer');?>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
