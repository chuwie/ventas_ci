<div class="container-fluid">
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Category-->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <form id="form-category">
                <input type="hidden" name="id_category" id="id_category"/> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">MANTENEDOR CATEGORIAS</h4>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-2 text-right control-label col-form-label">Nombre: *</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control validate[required]" id="name" name="name" placeholder="Nombre Categoría">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-2 text-right control-label col-form-label">Padre: </label>
                                    <div class="col-sm-9">
                                        <select id="category" name="category" class="select2 form-control custom-select">
                                            <!-- <option value="">Categoría Padre</option>   -->
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="border-top"  style="margin-bottom:2%">
                                <div class="card-body">
                                    <button type="button" class="btn btn-primary btn-right" id="btn-save">CREAR</button>
                                    <button type="button" class="btn btn-danger btn-left" onClick="Clean()">CANCELAR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12">
                        <table id="tcategory" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th width="100">Item</th>
                                    <th>Nombre</th>
                                    <th width="100">Modificar</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Category-->
    <!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
</div>