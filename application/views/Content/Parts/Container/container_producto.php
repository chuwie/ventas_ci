<div class="container-fluid">
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Product-->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <form id="form-product">
                <input type="hidden" name="id_product" id="id_product"/> 
                <div class="row">
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">CREAR PRODUCTO</h4>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-2 text-right control-label col-form-label">Nombre *</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control validate[required]" id="name" name="name" placeholder="Nombre del producto">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lname" class="col-sm-2 text-right control-label col-form-label">Precio *</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">$</span>
                                            </div>
                                            <input type="number" class="form-control validate[required]" id="precio" name="precio" value="1" min="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="cono1" class="col-sm-2 text-right control-label col-form-label">Fecha caducidad</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control date-inputmask" id="date-mask" name="fecha" placeholder="Enter Date">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="cono1" class="col-sm-2 text-right control-label col-form-label">Descripción</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control detail-product" name="detail" id="detail"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="button" class="btn btn-primary btn-right" onClick="SaveProduct()" id="btn-save">CREAR</button>
                                    <button type="button" class="btn btn-danger btn-left" onClick="Clean()">CANCELAR</button>
                                </div>
                            </div>
                        <br/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">ASIGNAR MARCA</h4>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <select class="select2 form-control custom-select" style="width: 100% !important; height:36px !important;" id="brand" name="brand">
                                            <option value="">SIN MARCA</option>
                                            <?php 
                                                if(is_array($brands)){
                                                    foreach($brands as $value){
                                                        echo '<option value="'.$value->id_marca.'">'.$value->detalle.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">ASIGNAR CATEGORÍA</h4>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <select class="select2 form-control custom-select" style="width: 100% !important; height:36px !important;" id="category" name="category">
                                            <option value="">SIN CATEGORÍA</option>
                                            <?php
                                                if(!empty($category)){
                                                    echo $category;
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body" style="margin-bottom: 65%;">
                                <h4 class="card-title">AGREGAR FOTOS</h4>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="custom-file choose-photo">
                                            <input type="file" class="custom-file-input" id="validatedCustomFile" accept="image/x-png, image/jpeg">
                                            <label class="custom-file-label" for="validatedCustomFile">Elegir archivo...</label>
                                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                                        </div>
                                        <div class="img-upload">
                                        <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12">
                        <table id="tproduct" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Precio</th>
                                    <th>Marca</th>
                                    <th>Categoría</th>
                                    <th>Caducidad</th>
                                    <th>Código Prod.</th>
                                    <th>Crear Código de Barra</th>
                                    <th width="100">Modicar</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Product-->
    <!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
</div>