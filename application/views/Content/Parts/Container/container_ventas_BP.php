<div class="container-fluid">
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Product-->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">DATOS CLIENTE</h4>
                            <div class="form-group row">
                                <label for="rut" class="col-sm-2 text-right control-label col-form-label">RUT *</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control validate[required]" id="rut" name="rut" placeholder="Ingrese RUT">
                                </div>
                                <label for="razon" class="col-sm-2 text-right control-label col-form-label">Razón Social *</label>
                                <div class="col-sm-4">
                                    <select class="select2 form-control custom-select" style="width: 100% !important; height:36px !important;" id="razon" name="razon">
                                        <option value="">SELECCIONAR</option>
                                        <option value="1">RAZÓN 1</option>
                                        <option value="2">RAZÓN 2</option>
                                        <option value="3">RAZÓN 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname" class="col-sm-2 text-right control-label col-form-label">Nombre *</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control validate[required]" id="name" name="name">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="direcction" class="col-sm-2 text-right control-label col-form-label">Dirección</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control validate[required]" id="direcction" name="direcction" placeholder="Dirección">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-search"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="producto" class="text-left control-label col-form-label custom-form-label">Producto</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="producto" name="producto"/>
                                </div>
                                <label for="cantidad" class="text-left control-label col-form-label custom-form-label">Cant.</label>
                                <div class="col-sm-1">
                                    <input type="text" class="form-control" id="cantidad" name="cantidad"/>
                                </div>
                                <label for="precio" class="text-rigth control-label col-form-label custom-form-label">Precio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" id="precio" name="precio"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-search btn-plus"><i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <table id="tventas" class="table table-striped table-bordered table-ventas">
                                        <thead>
                                            <tr>
                                                <th width="10">ID</th>
                                                <th width="200">PRODUCTO</th>
                                                <th width="70">CANTIDAD</th>
                                                <th width="70">VALOR</th>
                                                <th width="60">%</th>
                                                <th width="70">DESCUENTO</th>
                                                <th width="60">TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">DATOS VENTA</h4>
                            <div class="col-md-12">
                                <div class="form-group-venta row">
                                    <label for="ticket" class="col-sm-5 text-left control-label col-form-label">N° Ticket *</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="text" class="form-control validate[required]" id="ticket" name="ticket" value="1" min="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group-venta row">
                                    <label for="boleta" class="col-sm-5 text-left control-label col-form-label">N° Boleta *</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <input type="text" class="form-control validate[required]" id="boleta" name="boleta" value="1" min="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <div class="form-group-venta row">
                                    <label for="sub" class="col-sm-5 text-left control-label col-form-label">Sub Total </label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <!-- <input type="text" class="form-control validate[required]" id="sub" name="sub" value="1" min="1"> -->
                                             <label class="valor" id="sub">$ 0</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group-venta row">
                                    <label for="iva" class="col-sm-5 text-left control-label col-form-label">19% IVA</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <!-- <input type="text" class="form-control validate[required]" id="iva" name="iva" value="1" min="1"> -->
                                            <label class="valor" id="iva">$ 0</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <div class="form-group-venta row">
                                    <label for="total" class="col-sm-5 text-left control-label col-form-label">Total Pagar</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <!-- <input type="text" class="form-control validate[required]" id="iva" name="iva" value="1" min="1"> -->
                                            <label class="valor" id="total">$ 0</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="button" class="btn btn-primary btn-right" onClick="SaveProduct()" id="btn-save">CREAR</button>
                                <button type="button" class="btn btn-danger btn-left" onClick="Clean()">CANCELAR</button>
                            </div>
                        </div>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
    <!-- ============================================================== -->
    <!-- End Product-->
    <!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
</div>