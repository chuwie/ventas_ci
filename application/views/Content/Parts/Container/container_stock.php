<div class="container-fluid">
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
    <!-- <?php var_dump($user); ?> -->
    <!-- ============================================================== -->
    <!-- Stock-->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <form id="form-stock">
                <input type="hidden" name="id" id="id"/> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">AGREGAR STOCK</h4>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-2 text-right control-label col-form-label">Producto *</label>
                                    <div class="col-sm-9">
                                        <select id="product" name="product" class="select2 form-control custom-select validate[required]" style="width: 100% !important; height:36px !important;">
                                            <option value="">Búscar Producto</option>
                                            <?php
                                                if(!empty($products)){
                                                    foreach($products as $prod){
                                                        echo '<option value="'.$prod->id_producto.'">'.$prod->nombre.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="stock" class="col-sm-2 text-right control-label col-form-label">Stock *</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2"><i class="mdi mdi-numeric"></i></span>
                                            </div>
                                            <input type="number" class="form-control validate[required]" id="stock" name="stock" value="1" min="1">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-2 text-right control-label col-form-label">Sucursal *</label>
                                    <div class="col-sm-9">
                                        <select id="branchoffice" name="branchoffice" class="select2 form-control custom-select validate[required]" style="width: 100% !important; height:36px !important;">
                                            <option value="">Búscar Sucursar</option>
                                            <?php
                                                if(!empty($branchoffice)){
                                                    foreach($branchoffice as $branch){
                                                        echo '<option value="'.$branch->id_sucursal.'">'.$branch->direccion.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="button" class="btn btn-primary btn-right" onClick="SaveStock()" id="btn-save">CREAR</button>
                                    <button type="button" class="btn btn-danger btn-left" onClick="Clean()">CANCELAR</button>
                                </div>
                            </div>
                        <br/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12">
                        <table id="tstock" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th width="400">Dirección</th>
                                    <th width="100">Stock</th>
                                    <th width="100">Modificar</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Stock-->
    <!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
</div>