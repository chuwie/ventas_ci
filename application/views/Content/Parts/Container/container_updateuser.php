<div class="container-fluid">
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
    <!-- <?php var_dump($user); ?> -->
    <!-- ============================================================== -->
    <!-- Stock-->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <form id="form-updateuser">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">DATOS USUARIOS</h4>
                                <div class="form-group row">
                                    <label for="new" class="col-sm-2 text-right control-label col-form-label">Usuario *</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2"><i class="mdi mdi-account"></i></span>
                                            </div>
                                            <select id="account" name="account" class="form-control validate[required]">
                                                <option value="1">Admin</option>
                                                <option value="2">Ventas</option>
                                                <option value="3">Caja</option>
                                                <option value="4">Bodega</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="new" class="col-sm-2 text-right control-label col-form-label">Sucursal *</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2"><i class="fas fa-building"></i></span>
                                            </div>
                                            <select id="branchoffice" name="branchoffice" class="select2 form-control custom-select validate[required]" style="width: 95% !important; height:36px !important;">
                                                <option value="">Búscar Sucursal</option>
                                                <?php
                                                    if(!empty($branchoffice)){
                                                        foreach($branchoffice as $branch){
                                                            echo '<option value="'.$branch->id_sucursal.'">'.$branch->direccion.'</option>';
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="new" class="col-sm-2 text-right control-label col-form-label">Nueva contraseña *</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2"><i class="mdi mdi-key"></i></span>
                                            </div>
                                            <input type="text" class="form-control validate[required]" id="new" name="new">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-success" id="auto">Generar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="button" class="btn btn-primary btn-right" id="btn-save">ACTUALIZAR</button>
                                    <button type="button" class="btn btn-danger btn-left" onClick="Clean()">CANCELAR</button>
                                </div>
                            </div>
                        <br/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Stock-->
    <!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
</div>