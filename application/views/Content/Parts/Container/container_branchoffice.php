<div class="container-fluid">
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Branchoffice-->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <form id="form-branchoffice">
                <input type="hidden" name="id_branchoffice" id="id_branchoffice"/> 
                <div class="row">
                    <div class="col-md-9">
                        <div class="card" style="height: 95%;">
                            <div class="card-body">
                                <h4 class="card-title">CREAR SUCURSAL</h4>
                                <div class="form-group row">
                                    <label for="direction" class="col-sm-2 text-right control-label col-form-label">Dirección *</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2"><i class="mdi mdi-directions-fork"></i></span>
                                            </div>
                                            <input type="text" class="form-control validate[required]" id="direction" name="direction" placeholder="Dirección de la sucursal">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-sm-2 text-right control-label col-form-label">Teléfono *</label>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2"><i class="mdi mdi-cellphone"></i></span>
                                            </div>
                                            <input type="text" class="form-control validate[required]" id="phone" name="phone" placeholder="+56911223344">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="button" class="btn btn-primary btn-right" onClick="SaveBrandoffice()" id="btn-save">CREAR</button>
                                    <button type="button" class="btn btn-danger btn-left" onClick="Clean()">CANCELAR</button>
                                </div>
                            </div>
                        <br/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">AGREGAR FOTOS</h4>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="custom-file choose-photo">
                                            <input type="file" class="custom-file-input" id="validatedCustomFile">
                                            <label class="custom-file-label" for="validatedCustomFile">Elegir archivo...</label>
                                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                                        </div>
                                        <div class="img-upload">
                                            <div class="thumbnail-custom">
                                                <span class="delete-img">x</span>
                                                <img src="<?php echo base_url(); ?>include/assets/images/content/assassins.jpg" class="portrait" alt="Image" style="width: 90%"/>
                                            </div>
                                            <div class="thumbnail-custom">
                                                <span class="delete-img">x</span>
                                                <img src="<?php echo base_url(); ?>include/assets/images/content/assassins.jpg" class="portrait" alt="Image" style="width: 90%"/>
                                            </div>
                                        <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12">
                        <table id="tbranchoffice" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sucursal</th>
                                    <th width="100">Modificar</th>
                                    <th width="100">Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Branchoffice-->
    <!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
</div>