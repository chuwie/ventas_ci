<div class="container-fluid">
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Ventas-->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-md-8">
                    <div class="card" style="height: 740px;">
                        <div class="card-body">
                            <!-- <h4 class="card-title">DATOS CLIENTES</h4> -->
                            <div class="form-group row">
                                <label for="producto" class="col-sm-2 text-right control-label col-form-label">Producto</label>
                                <div class="col-sm-10">
                                    <select class="select2 form-control custom-select" style="width: 100% !important; height:36px !important;" id="producto" name="producto">
                                        <option value="">SELECCIONAR</option>
                                        <?php 
                                            foreach($products as $prod){
                                                echo '<option value="'.$prod->id_producto.'">'.$prod->nombre.'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <table id="tventas" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="10">ITEM</th>
                                                <th width="70">CANT</th>
                                                <th width="500">PROD</th>
                                                <th width="180">DESC</th>
                                                <th width="150">PRECIO</th>
                                                <th width="150">TOTAL</th>
                                                <th width="20">QUITAR</th>
                                            </tr>
                                        </thead>
                                        <tbody class="center-table">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="height: 740px;">
                        <div class="card-body">
                            <form id="form-ticket">
                                <!-- <input type="hidden" id="n_carro" name="n_carro" value="<?php echo explode('|', $ticket)[1] ?>" /> -->
                                <div class="form-group row">
                                    <div class="col-sm-12 div-total">
                                        <div class="div-price">
                                            $ <span id="total-price">0</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label for="nticket" class="text-left control-label col-form-label">N° Ticket</label>
                                        <input type="text" class="form-control validate[required]" id="nticket" name="nticket" value="<?php echo explode('|', $ticket)[0] ?>" disabled />
                                    </div>
                                    <!-- <div class="col-sm-6">
                                        <label for="nboleta" class="text-left control-label col-form-label">N° Boleta</label>
                                        <input type="text" class="form-control validate[required]" id="nboleta" name="nboleta" disabled />
                                    </div> -->
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="document" class="text-left control-label col-form-label">Documento</label>
                                        <select class="form-control" id="document" name="document">
                                            <option value="boleta">BOTELA</option>
                                            <option value="factura">FACTURA</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="pago" class="text-left control-label col-form-label">TIPO PAGO</label>
                                        <select class="form-control" id="pago" name="pago">
                                            <option value="efectivo">EFECTIVO</option>
                                            <option value="debito">DÉBITO</option>
                                            <option value="credito">CRÉDITO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label for="date" class="text-left control-label col-form-label">FECHA COMPRA</label>
                                        <input type="text" class="form-control validate[required]" id="date" name="date" disabled />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-5">
                                        <label for="rut" class="text-left control-label col-form-label">RUT CLIENTE</label>
                                        <input type="text" class="form-control input_rut" id="rut" name="rut" disabled/>
                                    </div>
                                    <div class="col-sm-7">
                                        <label for="giro" class="text-left control-label col-form-label">GIRO CLIENTE</label>
                                        <input type="text" class="form-control" id="giro" name="giro" disabled/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label for="razon" class="text-left control-label col-form-label">NOMBRE CLIENTE</label>
                                        <input type="text" class="form-control" id="razon" name="razon" disabled/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label for="direction" class="text-left control-label col-form-label">DIRECCIÓN CLIENTE</label>
                                        <input type="text" class="form-control" id="direction" name="direction" disabled/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary btn-right" id="btn-ticket" style="margin-top: -2%;">CREAR TICKET</button>
                                    <button type="button" class="btn btn-danger btn-left" onClick="Clean()" style="margin-top: -2%;">CANCELAR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Ventas-->
    <!-- ============================================================== -->

<!-- ============================================================== -->
<!-- End Page Content -->
<!-- ============================================================== -->
</div>