<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <?php
        
        $this->load->view('Content/Parts/topbar');
        $this->load->view('Content/Parts/leftsidebar');
        $this->load->view('Content/Parts/wrapper'); 
    ?>
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->