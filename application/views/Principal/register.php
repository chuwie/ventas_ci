<!DOCTYPE html>
<html dir="ltr" lang="es">

<?php $this->load->view('Content/Header/header'); ?>
<body>
    <?php $this->load->view('Content/Register/main_register'); ?>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php 
        $this->load->view('Content/Modals/loadmodal');
        $this->load->view('Content/Load/load'); 
    ?>
    <script src="<?php base_url(); ?>include/js/privates/register.js"></script>
    <script src="<?php base_url(); ?>include/js/privates/codigospaises.js"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $(".preloader").fadeOut();
    </script>
</body>

</html>