<!DOCTYPE html>
<html dir="ltr" lang="es">

<?php $this->load->view('Content/Header/header'); ?>
<body>
    <?php $this->load->view('Content/Login/main_login'); ?>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php 
        $this->load->view('Content/Modals/loadmodal');
        $this->load->view('Content/Load/load'); 
    ?>
    <script src="<?php base_url(); ?>include/js/privates/login.js"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $(".preloader").fadeOut();
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });
        $('#to-login').click(function(){
            
            $("#recoverform").hide();
            $("#loginform").fadeIn();
        });
    </script>
</body>

</html>