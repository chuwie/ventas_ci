<!DOCTYPE html>
<html dir="ltr" lang="es">

<?php $this->load->view('Content/Header/header'); ?>
<body>
    <?php $this->load->view('Content/Main/main'); ?>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php 
        $this->load->view('Content/Modals/loadmodal');
        $this->load->view('Content/Load/load'); 
    ?>
    <script src="<?php base_url(); ?>include/js/privates/marcas.js"></script>
</body>

</html>