<?php 
    if($user['tipo_user']==0){
?>
<!DOCTYPE html>
<html dir="ltr" lang="es">

<?php $this->load->view('Content/Header/header'); ?>
<body>
    <?php $this->load->view('Content/Main/main'); ?>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php 
        $this->load->view('Content/Modals/loadmodal');
        $this->load->view('Content/Load/load'); 
    ?>
</body>
    <script src="<?php base_url(); ?>include/js/privates/sucursal.js"></script>
</html>
<?php }else{ 
    $this->load->view('Principal/error');
}?>