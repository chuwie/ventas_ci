<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mailer{
    
    public function SendMail($info, $email_to, $name, $tipo){
        header('Content-Type: application/json');

        require_once APPPATH . 'libraries/PHPMailer/src/Exception.php';
        require_once APPPATH . 'libraries/PHPMailer/src/PHPMailer.php';
        require_once APPPATH . 'libraries/PHPMailer/src/SMTP.php';

        $mail = new PHPMailer(true);
        try {
            switch($tipo){
                case 'new':
                            $body = 'Bienvenido a nuestra plataforma, te entregamos las credenciales asociadas a tu sucursal: </br></br>';
                            foreach($info as $value){
                                $body .= '<b>Tipo: </b>'.$value['tipo'].'</br>';
                                $body .= '<b>Usuario: </b>'.$value['usuario'].'</br>';
                                $body .= '<b>Contraseña: </b>'.$value['password'].'</br></br>';
                            }
                            $body .= 'También le otorgamos el link de su "Panel de Administrador".</br></br>';
                            $body .= 'Link: <a href="http://localhost/ventas_ci/login">http://localhost/ventas_ci/login</a> </br></br>';
                            $body .= 'Para más información o para realizar consultas, escribanos a moralesjaraf@gmail.com o llame al (+56) 940220374. </br>';
                            $body .= 'Gracias por su registrarse.';
                    break;
                case 'create':
                            $body = 'Te entregamos las credenciales asociadas a tu sucursal: </br></br>';
                            foreach($info as $value){
                                $body .= '<b>Tipo: </b>'.$value['tipo'].'</br>';
                                $body .= '<b>Usuario: </b>'.$value['usuario'].'</br>';
                                $body .= '<b>Contraseña: </b>'.$value['password'].'</br></br>';
                            }
                            $body .= 'También le otorgamos el link de su "Panel de Administrador".</br></br>';
                            $body .= 'Link: <a href="http://localhost/ventas_ci/login">http://localhost/ventas_ci/login</a> </br></br>';
                            $body .= 'Para más información o para realizar consultas, escribanos a moralesjaraf@gmail.com o llame al (+56) 940220374. </br>';
                            $body .= 'Gracias por su registrarse.';
                    break;
                case 'update':
                            $body = 'Los datos del usuario han sido actaulizados: </br></br>';
                            foreach($info as $value){
                                $body .= '<b>Tipo: </b>'.$value['tipo'].'</br>';
                                $body .= '<b>Usuario: </b>'.$value['usuario'].'</br>';
                                $body .= '<b>Contraseña: </b>'.$value['password'].'</br></br>';
                            }
                            $body .= 'Para más información o para realizar consultas, escribanos a moralesjaraf@gmail.com o llame al (+56) 940220374. </br>';
                            $body .= 'Gracias por su registrarse.';
                    break;
            }
            //Server settings
            // $mail->SMTPDebug = 2;                                    // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = 'moralesjaraf@gmail.com';               // SMTP username
            $mail->Password   = 'hrjzuntkegachyxg';                     // SMTP password
            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('moralesjaraf@gmail.com', 'Felipe Morales');
            $mail->addAddress($email_to, $name);                        // Add a recipient
            // $mail->addAddress('ellen@example.com');                     // Name is optional
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            // Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');               // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');          // Optional name

            // Content
            $mail->isHTML(true);                                           // Set email format to HTML
            $mail->Subject = 'Bienvenido - Credenciales';
            $mail->Body    = $body;
            // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $mail->CharSet = 'UTF-8';
            
            if($mail->send()){
                return true;
            }else{
                return false;
            }
        }catch(Exception $e){
            // echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            return false;
        }
    }
}
?>