<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class category extends CI_Controller {

	private $varSession;

	function __construct(){
		parent::__construct();
		$this->load->model('mcategory');
		$this->varSession = $this->session->userdata('user');
	}
	
	public function index(){
		if(isset($this->varSession)){
			$data['user'] = $this->varSession;
			$data['content'] = 'categoria';
			$this->load->view('Principal/category', $data);
		}else{
			$this->load->view('Principal/error');
		}
	}

	public function CreateCategory(){
		header('Content-Type: application/json');

		$id_category = $this->input->post('id_category');
		$name = $this->input->post('name');
		$category = $this->input->post('category');

		if(empty($id_category)){
			$data = array(
				'name'=>$name,
				'category'=>$category,
				'entidad'=>$this->varSession['entidad']
			);
			$datos = $this->mcategory->InsertCategory($data);	
		}else{
			$data = array(
				'id_category'=>$id_category,
				'name'=>$name,
				'category'=>$category,
				'entidad'=>$this->varSession['entidad']
			);
			$datos = $this->mcategory->ModifyCategory($data);
		}
		echo json_encode($datos);
	}

	public function LoadCategory(){
		header('Content-Type: application/json');
		$datos = $this->mcategory->GetAllCategory($this->varSession['entidad']);

		$options='<option value="">Categoría padre</option>';
		if(!empty($datos)){
			foreach($datos as $value){
				$options .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
				$options .= $this->CreteChildren($value);
			}
		}
		echo json_encode($options);
	}

	private function CreteChildren($value, $options = '', $espacios=''){
		// print_r($value);
		$espacios = '&nbsp;&nbsp;&nbsp;'.$espacios;
		$options='';
		if(isset($value['children'])){
			foreach($value['children'] as $child){
				$options .= '<option value="'.$child['id'].'">'.$espacios.$child['name'].'</option>';
				if(isset($child['children'])){
					$options .= $this->CreteChildren($child, $options, $espacios);
				}
			}
		}
		return $options;
	}
	
	public function LoadTableCategory(){
		header('Content-Type: application/json');
		$datos = $this->mcategory->GetAllCategory($this->varSession['entidad']);
		$tree='';
		if(!empty($datos)){
			$tree = $this->CreteTableTree($datos);
		}
		echo json_encode($tree);
	}

	private function CreteTableTree($array, $espacios=''){
		$espacios = '&nbsp;&nbsp;&nbsp;'.$espacios;
		$new = array();
		if(!empty($array)){
			foreach($array as $value){
				$new[] = array('id'=>$value['id'], 'name'=>$espacios.'<i class="fas fa-arrow-right" />&nbsp;'.$value['name'], 'modify'=>'<button class="btn btn-primary" onClick="ModifyCategory('.$value['id'].', \''.$value['name'].'\', '.$value['parentid'].', event)">Modificar</button>');
				if(isset($value['children'])){
					$new = array_merge($new, $this->CreteTableTree($value['children'], $espacios));
				}
			}
		}
		return $new;
	}
	
}
?>