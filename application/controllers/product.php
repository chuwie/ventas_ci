<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class product extends CI_Controller {
	private $varSession;

	function __construct(){
		parent::__construct();
		$this->load->model('mproduct');
		$this->varSession = $this->session->userdata('user');
	}
	
	public function index(){
		if(isset($this->varSession)){
			$data=array(
				'user'=>$this->varSession,
				'content'=>'producto',
				'brands'=>$this->mproduct->GetBrands($this->varSession['entidad']),
				'category'=>$this->LoadCategory()
			);
			$this->load->view('Principal/productos', $data);
		}else{
			$this->load->view('Principal/error');
		}
	}

	public function CreateProduct(){
		header('Content-Type: application/json');

		$id_product = $this->input->post('id_product');

		if(empty($id_product)){
			$data = array(
				'name'=>$this->input->post('name'),
				'price'=>$this->input->post('precio'),
				'detail'=>$this->input->post('detail'),
				'date'=>$this->input->post('fecha'),
				'brand'=>$this->input->post('brand'),
				'category'=>$this->input->post('category'),
				'entidad'=>$this->varSession['entidad']
			);
			$datos = $this->mproduct->InsertProduct($data);	
		}else{
			$data = array(
				'id_product'=>$id_product,
				'name'=>$this->input->post('name'),
				'price'=>$this->input->post('precio'),
				'detail'=>$this->input->post('detail'),
				'date'=>$this->input->post('fecha'),
				'brand'=>$this->input->post('brand'),
				'category'=>$this->input->post('category'),
				'entidad'=>$this->varSession['entidad']
			);
			$datos = $this->mproduct->ModifyProduct($data);
		}
		echo json_encode($datos);
	}

	public function LoadProduct(){
		header('Content-Type: application/json');
		$params = $_REQUEST;
		$records = $params['length'];
		$page = ($params['start'] / $records) + 1;
		$search = $params['search']['value'];

		$data = array(
			'search'=>$search,
			'page'=>$page,
			'records'=>$records,
			'entidad'=>$this->varSession['entidad']
		);
		$info = $this->mproduct->ProductTable($data);
		if($info != false){
			$nrecords = $this->mproduct->GetTotalProduct($data);
			foreach($info as $value){
				if($value->id_estado == 1)
					$state = '<div class="center-toggle"><label class="switch"><input type="checkbox" checked name="toggle-product" onChange="ChangeState($(this), '.$value->id_producto.')" ><span class="slider round"></span></label></div>';
				else if($value->id_estado == 2)
					$state = '<div class="center-toggle"><label class="switch"><input type="checkbox" name="toggle-product" onChange="ChangeState($(this), '.$value->id_producto.')" ><span class="slider round"></span></label></div>';
				$detalle=$this->br2nl($value->detalle);
				$tabla[] = array(
					'name'=>$value->nombre,
					'price'=>'$ '.number_format($value->precio, 0, '', '.'),
					'brand'=>$value->marca,
					'category'=>$value->categoria,
					'date'=>$value->fecha_caducidad,
					'code'=>$value->codigo_barra,
					'bar'=>'<button type="button" class="btn btn-primary" onClick="DownloadBarcode(\''.$value->codigo_barra.'\', event)">Descargar</button>',
					// 'bar'=>'<a class="btn btn-primary" href="CreateBarcode('.$value->codigo_barra.')" >Descargar</a>',
					'modify'=>'<button type="button" class="btn btn-primary" onClick="ModifyProducto('.$value->id_producto.', \''.$value->nombre.'\', '.$value->precio.' ,\''.$value->fecha_caducidad.'\', \''.$detalle.'\', '.$value->id_categoria.', '.$value->id_marca.', event)">MODIFICAR</button>',
					'state'=>$state
				);
			}
			$json_data=array(
				'draw'=>intval($params['draw']),
				'recordsTotal'=>$nrecords,
			    'recordsFiltered'=>$nrecords,
			    'data'=>$tabla
			);
		}else{
			$tabla[] = array('name'=>'', 'price'=>'', 'brand'=>'', 'category'=>'', 'date'=>'', 'code'=>'', 'bar'=>'', 'modify'=>'', 'state'=>'');
			$json_data=array(
				'draw'=>intval($params['draw']),
				'recordsTotal'=>0,
			    'recordsFiltered'=>0,
			    'data'=>$tabla
			);
		}
		echo json_encode($json_data);
	}

	public function ChangeStateProduct(){
		header('Content-Type: application/json');

		if($this->input->post('state')=='true')
			$state=1;
		else
			$state=2;

		$data = array(
			'id_product'=>$this->input->post('product'),
			'state'=>$state,
			'entidad'=>$this->entidad
		);
		$datos = $this->mproduct->ChangeStateProduct($data);
		echo json_encode($datos);
	}

	private function LoadCategory(){
		$datos = $this->mproduct->GetAllCategory($this->varSession['entidad']);
		$options='';
		if(!empty($datos)){
			foreach($datos as $value){
				$options .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
				$options .= $this->CreteChildren($value);
			}
		}
		return $options;
	}

	private function CreteChildren($value, $options = '', $espacios=''){
		$espacios = '&nbsp;&nbsp;&nbsp;'.$espacios;
		$options='';
		if(isset($value['children'])){
			foreach($value['children'] as $child){
				$options .= '<option value="'.$child['id'].'">'.$espacios.$child['name'].'</option>';
				if(isset($child['children'])){
					$options .= $this->CreteChildren($child, $options, $espacios);
				}
			}
		}
		return $options;
	}

	private function br2nl($str){
		$str = preg_replace("/(\r\n|\n|\r)/", "", $str);
		return preg_replace("=&lt;br */?&gt;=i", "\n", $str);
	}

	public function CreateBarcode(){
		header('Content-Type: application/json');
		include APPPATH . 'libraries\Barcode\barcode.php';
		include APPPATH . 'libraries\PDF\fpdf.php';
		$code = $this->input->post('codigo');
		// $path = 'include/codigo/codigo'.$code.'.png';
		$ruta = 'include/codigo/codigos_'.$this->varSession['entidad'];
		if(!file_exists($ruta)){
			mkdir($ruta, 0777);
		}
		$path = $ruta.'/codigo'.$code.'.png';
		barcode($path, $code, 20, 'horizontal', 'code128', true);
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 20);
		$pdf->Image($path,10,10,50,0,'PNG');
		// $document = $pdf->Output();
		// $link = 'window.open("'.$pdf->Output().'", "_blank")';
		echo json_encode($path);
	}

}
?>