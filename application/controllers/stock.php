<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class stock extends CI_Controller {
	private $varSession;

	function __construct(){
		parent::__construct();
		$this->load->model('mstock');
		$this->varSession = $this->session->userdata('user');
	}
	
	public function index(){
		if(isset($this->varSession)){
			$data=array(
				'user'=>$this->varSession,
				'content'=>'stock',
				'products'=>$this->LoadProducts(),
				'branchoffice'=>$this->LoadBranchoffice()
			);
			$this->load->view('Principal/stock', $data);
		}else{
			$this->load->view('Principal/error');
		}
	}

	public function LoadProducts(){
		$data = array(
			'entidad'=>$this->varSession['entidad']
		);
		$info = $this->mstock->ProductsList($data);
		if($info != false){
			return $info;
		}else{
			return '';
		}
	}

	public function LoadBranchoffice(){
		if(!empty($this->varSession['sucursal']))
			$branchoffice = $this->varSession['sucursal'];
		else
			$branchoffice = '';

		$data = array(
			'branchoffice'=>$branchoffice,
			'entidad'=>$this->varSession['entidad']
		);
		$info = $this->mstock->BranchofficeList($data);
		if($info != false){
			return $info;
		}else{
			return '';
		}
	}

	public function CreateStockProduct(){
		header('Content-Type: application/json');

		$id = $this->input->post('id');
		$product = $this->input->post('product');
		$stock = $this->input->post('stock');
		$branchoffice = $this->input->post('branchoffice');

		if(!empty($id)){
			$data = array(
				'id'=>$id,
				'product'=>$product,
				'stock'=>$stock,
				'branchoffice'=>$branchoffice
			);
			$datos = $this->mstock->ModifyStockProduct($data);
			echo json_encode($datos);
		}else{
			$data = array(
				'product'=>$product,
				'stock'=>$stock,
				'branchoffice'=>$branchoffice
			);
			$datos = $this->mstock->InsertStockProduct($data);
			echo json_encode($datos);
		}	
	}

	public function LoadStockProduct(){
		header('Content-Type: application/json');
		$ntotal_registros = 0;
		$params = $_REQUEST;
		$records = $params['length'];
		$page = ($params['start'] / $records) + 1;
		$search = $params['search']['value'];

		$data = array(
			'search'=>$search,
			'page'=>$page,
			'records'=>$records,
			'entidad'=>$this->varSession['entidad']
		);
		$info = $this->mstock->StockProductTable($data);
		if($info != false){
			$nrecords = $this->mstock->GetTotalStockProduct($data);
			foreach($info as $value){
				$tabla[] = array(
					'name'=>$value->nombre,
					'direction'=>$value->direccion,
					'stock'=>$value->stock,
					'modify'=>'<button class="btn btn-primary" onClick="ModifyStockProducto('.$value->id_producto_sucursal.', '.$value->id_producto.', '.$value->id_sucursal.', '.$value->stock.', event)">MODIFICAR</button>'
				);
			}
			$json_data=array(
				'draw'=>intval($params['draw']),
				'recordsTotal'=>$nrecords,
			    'recordsFiltered'=>$nrecords,
			    'data'=>$tabla
			);
		}else{
			$tabla[] = array('name'=>'', 'direction'=>'', 'stock'=>'', 'modify'=>'');
			$json_data=array(
				'draw'=>intval($params['draw']),
				'recordsTotal'=>0,
			    'recordsFiltered'=>0,
			    'data'=>$tabla
			);
		}
		echo json_encode($json_data);
	}
}
?>