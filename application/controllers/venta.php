<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class venta extends CI_Controller {

	private $varSession;
	private $varTicket;
	function __construct(){
		parent::__construct();
		$this->varSession = $this->session->userdata('user');
		$this->varTicket = $this->session->userdata('ticket');
	}

	public function index(){
		if(isset($this->varSession)){
			$data = array(
				'content'=>'venta',
				'user'=>$this->varSession,
				'products'=>$this->GetListProduct(),
				'ticket'=>$this->GetTicket()
			);
			$this->load->view('Principal/ventas', $data);
		}else{
			$this->load->view('Principal/error');
		}
	}

	private function GetTicket(){
		if(!isset($this->varTicket)){
			$data = array(
				'entidad'=>$this->varSession['entidad']
			);
			$info = $this->ventas->GetNumTicket($data);
			$this->session->set_userdata('ticket', $info);
		}
		return $this->session->userdata('ticket');
	}

	public function GetDataUser(){
		header('Content-Type: application/json');

		$data = array(
			'rut'=>$this->input->post('rut'),
			'entidad'=>$this->varSession['entidad']
		);
		$info = $this->ventas->DataUser($data);
		echo json_encode($info);
	}

	private function GetListProduct(){
		$data = array(
			'sucursal'=>$this->varSession['sucursal'],
			'entidad'=>$this->varSession['entidad']
		);
		$info = $this->ventas->ListProduct($data);
		return $info;
	}

	public function GetDataProduct(){
		header('Content-Type: application/json');

		$data = array(
			'id_producto'=>$this->input->post('id_producto'),
			'entidad'=>$this->varSession['entidad']
		);
		$info = $this->ventas->GetInfoProducts($data);
		echo json_encode($info);
	}

	public function SaveTicketClient(){
		header('Content-Type: application/json');

		$form = $this->input->post('form');
		$sep = explode('&', $form);
		$datos=[];
		foreach($sep as $value){
			$explode = explode('=', $value);
			$datos[$explode[0]] = $explode[1];
		}

		$prods = $this->input->post('aProd');
		$total='';
		foreach($prods as $prod){
			$data = array(
				'id_producto'=>$prod['id_producto'],
				'entidad'=>$this->varSession['entidad']
			);
			$info = $this->ventas->GetInfoProducts($data);
			$descuento=0;
			$monto=0;
			$t=0;
			if(!empty($prod['descuento'])){
				$monto = (intval($info[0]->precio) * intval($prod['cantidad']));
				$descuento = ($prod['descuento'] * $monto) / 100;
				$t=($monto - $descuento);
				$total = $total + ($monto - $descuento);
			}else{
				$monto = (intval($info[0]->precio) * intval($prod['cantidad']));
				$t=$monto;
				$total = $total + $monto;
			}
			$detail[] = array(
				'id_producto'=>$prod['id_producto'],
				'cantidad'=>$prod['cantidad'],
				'descuento'=>$descuento,
				'monto'=>$monto,
				'total'=>$t
			);
		}
		$datos['id_carro'] = explode('|', $this->varTicket)[1];
		$datos['total_venta'] = $total;
		if(!isset($datos['rut'])){
			$datos['id_cliente'] = 1;
		}else{
			$datos['id_cliente'] = $this->ventas->GetIdClient($datos['rut']);
		}
		$datos['tipo_cliente'] = 1;
		$datos['prods'] = $detail;
		$datos['entidad'] = $this->varSession['entidad'];
		$result = $this->ventas->CreateTicket($datos);
		if($result){
			$return = array('message'=>'Ticket creado', 'response'=>true);
			$this->session->unset_userdata('ticket');
		}else{
			$return = array('message'=>'Problemas al tratar de crear el ticket, vuelva a intentarlo', 'response'=>false);
		}
		echo json_encode($return);
	}
	
}
?>