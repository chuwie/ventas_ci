<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

	private $varSession;

	function __construct(){
		parent::__construct();
		$this->load->model('mlogin');
		$this->varSession = $this->session->userdata('user');
	}
	
	public function index(){
		// print_r($this->varSession);
		if(!isset($this->varSession)){
			$this->load->view('Principal/login');
		}else{
			$data['content'] = 'venta';
			$data['user'] = $this->varSession;
			$this->load->view('Principal/ventas', $data);
		}
	}

	public function UserValidate(){
		header('Content-Type: application/json');

		$data = array(
			'email'=>$this->input->post('email'),
			'username'=>$this->input->post('username'),
			'password'=>$this->input->post('password')
		);
		$datos = $this->mlogin->IsValid($data);
		$response = $datos['response'];
		if($response){
			//crear session para el usuario y enviar url para redireccionar.
			$info=array(
				'message'=>base_url().$datos['message'], 
				'response'=>true
			);
			$user=array(
				'entidad'=>$datos['entidad'], 
				'tipo_user'=>$datos['tipo_user'], 
				'sucursal'=>$datos['sucursal'], 
				'name'=>$this->input->post('username'),
				'email'=>$this->input->post('email'),
				'list'=>$datos['list']
			);
			$this->session->set_userdata('user', $user);
		}else{
			$info = $datos;
		}
		echo json_encode($info);
	}

	public function Logout(){
		$this->session->unset_userdata('user');
		$this->session->unset_userdata('ticket');
		$this->load->view('Principal/login');
	}

}
?>