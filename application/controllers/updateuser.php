<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class updateuser extends CI_Controller {
	private $varSession;

	function __construct(){
		parent::__construct();
		$this->load->model('mupdateuser');
		$this->varSession = $this->session->userdata('user');
	}
	
	public function index(){
		if(isset($this->varSession)){
			$data=array(
				'user'=>$this->varSession,
				'content'=>'update',
				'branchoffice'=>$this->LoadBranchoffice()
			);
			$this->load->view('Principal/updateuser', $data);
		}else{
			$this->load->view('Principal/error');
		}
	}

	private function LoadBranchoffice(){
		if($this->varSession['sucursal']!=0)
			$branchoffice = $this->varSession['sucursal'];
		else
			$branchoffice = '';

		$data = array(
			'branchoffice'=>$branchoffice,
			'entidad'=>$this->varSession['entidad']
		);
		$info = $this->mupdateuser->BranchofficeList($data);
		if($info != false){
			return $info;
		}else{
			return '';
		}
	}

	public function ChangePassword(){
		header('Content-Type: application/json');
		$data = array(
			'account'=>$this->input->post('account'),
			'branchoffice'=>$this->input->post('branchoffice'),
			'newpass'=>$this->input->post('new'),
			'email'=>$this->varSession['email'],
			'name'=>$this->varSession['name'],
			'entidad'=>$this->varSession['entidad']
		);
		$datos = $this->mupdateuser->ModifyUser($data);
		echo json_encode($datos);
	}
	
}
?>