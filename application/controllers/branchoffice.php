<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class branchoffice extends CI_Controller {
	private $varSession;

	function __construct(){
		parent::__construct();
		$this->load->model('mbranchoffice');
		$this->varSession = $this->session->userdata('user');
	}
	
	public function index(){
		if(isset($this->varSession)){
			$data=array(
				'user'=>$this->varSession,
				'content'=>'branchoffice',
			);
			$this->load->view('Principal/branchoffice', $data);
		}else{
			$this->load->view('Principal/error');
		}
	}

	public function CreateBranchoffice(){
		header('Content-Type: application/json');

		$id_branchoffice = $this->input->post('id_branchoffice');
		$direction = $this->input->post('direction');
		$telefono = $this->input->post('phone');

		if(!empty($id_branchoffice)){
			$data = array(
				'id_branchoffice'=>$id_branchoffice,
				'direction'=>$direction,
				'phone'=>$telefono,
				'entidad'=>$this->varSession['entidad']
			);
			$datos = $this->mbranchoffice->ModifyBranchoffice($data);
			echo json_encode($datos);
		}else{
			$data = array(
				'direction'=>$direction,
				'phone'=>$telefono,
				'email'=>$this->varSession['email'],
				'name'=>$this->varSession['name'],
				'entidad'=>$this->varSession['entidad']
			);
			$datos = $this->mbranchoffice->InsertBranchoffice($data);
			echo json_encode($datos);
		}
	}

	public function LoadBranchoffice(){
		header('Content-Type: application/json');
		$ntotal_registros = 0;
		$params = $_REQUEST;
		$records = $params['length'];
		$page = ($params['start'] / $records) + 1;
		$search = $params['search']['value'];

		$data = array(
			'search'=>$search,
			'page'=>$page,
			'records'=>$records,
			'entidad'=>$this->varSession['entidad']
		);
		$info = $this->mbranchoffice->BranchofficeTable($data);
		if($info != false){
			$nrecords = $this->mbranchoffice->GetTotalBranchoffice($data);
			foreach($info as $value){
				if($value->id_estado == 1)
					$state = '<div class="center-toggle"><label class="switch"><input type="checkbox" checked name="toggle" onChange="ChangeState($(this), '.$value->id_sucursal.')" ><span class="slider round"></span></label></div>';
				else if($value->id_estado == 2)
					$state = '<div class="center-toggle"><label class="switch"><input type="checkbox" name="toggle" onChange="ChangeState($(this), '.$value->id_sucursal.')" ><span class="slider round"></span></label></div>';
				$tabla[] = array(
					'branchoffice'=>$value->direccion,
					'modify'=>'<button class="btn btn-primary" onClick="ModifyBranchoffice('.$value->id_sucursal.', \''.$value->direccion.'\', event)">MODIFICAR</button>',
					'state'=>$state
				);
			}
			$json_data=array(
				'draw'=>intval($params['draw']),
				'recordsTotal'=>$nrecords,
			    'recordsFiltered'=>$nrecords,
			    'data'=>$tabla
			);
		}else{
			$tabla[] = array('branchoffice'=>'', 'modify'=>'', 'state'=>'');
			$json_data=array(
				'draw'=>intval($params['draw']),
				'recordsTotal'=>0,
			    'recordsFiltered'=>0,
			    'data'=>$tabla
			);
		}
		echo json_encode($json_data);
	}

	public function ChangeStateBranchoffice(){
		header('Content-Type: application/json');

		$branchoffice = $this->input->post('branchoffice');
		$state = $this->input->post('state');

		if($state=='true')
			$state=1;
		else
			$state=2;

		$data = array(
			'branchoffice'=>$branchoffice,
			'state'=>$state,
			'entidad'=>$this->varSession['entidad']
		);
		$datos = $this->mbranchoffice->ChangeStateBranchoffice($data);
		echo json_encode($datos);
	}
}
?>