<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class brands extends CI_Controller {

	private $varSession;

	function __construct(){
		parent::__construct();
		$this->load->model('mbrands');
		$this->varSession = $this->session->userdata('user');
	}
	
	public function index(){
		if(isset($this->varSession)){
			$data['user'] = $this->varSession;
			$data['content'] = 'marcas';
			$this->load->view('Principal/brands', $data);
		}else{
			$this->load->view('Principal/error');
		}
    }
    
    public function CreateBrands(){
        header('Content-Type: application/json');

		$id_brands = $this->input->post('id_brands');
		$name = $this->input->post('name');

		if(empty($id_brands)){
			$data = array(
				'name'=>$name,
				'entidad'=>$this->varSession['entidad']
			);
			$datos = $this->mbrands->InsertBrands($data);	
		}else{
			$data = array(
				'id_brands'=>$id_brands,
				'name'=>$name,
				'entidad'=>$this->varSession['entidad']
			);
			$datos = $this->mbrands->ModifyBrands($data);
		}
		echo json_encode($datos);
	}
	
	public function LoadBrands(){
		header('Content-Type: application/json');
		$params = $_REQUEST;
		$records = $params['length'];
		$page = ($params['start'] / $records) + 1;
		$search = $params['search']['value'];

		$data = array(
			'search'=>$search,
			'page'=>$page,
			'records'=>$records,
			'entidad'=>$this->varSession['entidad']
		);
		$info = $this->mbrands->GetBrands($data);
		if($info != false){
			$nrecords = $this->mbrands->GetTotalBrands($data);
			foreach($info as $value){
				if($value->id_estado == 1)
					$state='<div class="center-toggle"><label class="switch"><input type="checkbox" checked name="toggle" onChange="ChangeState($(this), '.$value->id_marca.')" ><span class="slider round"></span></label></div>';
				else if($value->id_estado == 2)
					$state='<div class="center-toggle"><label class="switch"><input type="checkbox" name="toggle" onChange="ChangeState($(this), '.$value->id_marca.')" ><span class="slider round"></span></label></div>';
					
				$tabla[] = array(
					'name'=>$value->detalle,
					'modify'=>'<button class="btn btn-primary" onClick="ModifyBrands('.$value->id_marca.', \''.$value->detalle.'\', event)">MODIFICAR</button>',
					'state'=>$state
				);
			}
			$json_data=array(
				'draw'=>intval($params['draw']),
				'recordsTotal'=>$nrecords,
			    'recordsFiltered'=>$nrecords,
			    'data'=>$tabla
			);
		}else{
			$tabla[] = array('name'=>'', 'modify'=>'', 'state'=>'');
			$json_data=array(
				'draw'=>intval($params['draw']),
				'recordsTotal'=>0,
			    'recordsFiltered'=>0,
			    'data'=>$tabla
			);
		}
		echo json_encode($json_data);
	}

	public function ChangeStateBrands(){
		header('Content-Type: application/json');

		$brands = $this->input->post('brands');
		$state = $this->input->post('state');

		if($state=='true')
			$state=1;
		else
			$state=2;

		$data = array(
			'id_brands'=>$brands,
			'state'=>$state,
			'entidad'=>$this->varSession['entidad']
		);
		$datos = $this->mbrands->ChangeState($data);
		echo json_encode($datos);
	}


}
?>