<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class register extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('mregister');
	}
	
	public function index(){
		$validate = $this->ValidateUrl();
		if($validate){
			$this->load->view('Principal/register');
		}else{
			$this->load->view('Principal/error');
		}
		
	}

	public function CreateEntity(){
		header('Content-Type: application/json');

		$prefijo1 = '+'.$this->input->post('code1');
		$prefijo2 = '+'.$this->input->post('code2');

		$data = array(
			'name'=>$this->input->post('name'),
			'surnames'=>$this->input->post('surnames'),
			'email'=>$this->input->post('email'),
			'personalphone'=>$prefijo1.$this->input->post('personalphone'),
			'username'=>$this->input->post('username'),
			'password'=>$this->input->post('password'),
			'company'=>$this->input->post('company'),
			'direction'=>$this->input->post('direction'),
			'rut'=>$this->input->post('rut'),
			'phone'=>$prefijo1.$this->input->post('phone')
		);
		$datos = $this->mregister->RegisterEntity($data);
		echo json_encode($datos);
	}

	private function ValidateUrl(){
		$code = $this->input->get('code');
		if(!isset($code)){
			return false;
		}else{
			$datos = $this->mregister->CodeValidate($code);
			return $datos;
		}
	}
}
?>