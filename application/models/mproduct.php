<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mproduct extends CI_Model{

	public function __construct(){
		parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->database();
	}

	public function InsertProduct($data){
		$validate = $this->ValidateProduct($data['name'], $data['entidad']);
        if(!$validate){
			if(!empty($data['date'])){
				$date = $this->FormatDate($data['date']);
			}else{
				$date='';
			}
			$fecha = new DateTime();
			$codigo = 'prod'.$fecha->getTimestamp();
			$insert = array(
				'id_categoria'=>$data['category'],
				'id_marca'=>$data['brand'],
				'nombre'=>$data['name'],
				'precio'=>$data['price'],
				'detalle'=>$data['detail'],
				'codigo_barra'=>$codigo,
				'fecha_caducidad'=>$date,
				'id_entidad'=>$data['entidad']
			);
            $this->db->insert('pr_producto', $insert);
            if($this->db->affected_rows() != 1){
                return array('message'=>'ERROR AL TRATAR DE INGRESAR EL PRODUCTO', 'response'=>false);
            }else{
                return array('message'=>'PRODUCTO INGRESADO', 'response'=>true);
            }
        }else{
            return array('message'=>'YA HA INGRESADO ESTE PRODUCTO', 'response'=>false);
        }
	}

	private function ValidateProduct($name, $entidad){
        try{
            $where = array('nombre'=>$name, 'id_entidad'=>$entidad);
            $this->db->select('count(id_producto) as total');
            $this->db->from('pr_producto');
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()[0]->total > 0){
                return true;
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
        }
    }

	public function ModifyProduct($data){
		try{
			if(!empty($data['date'])){
				$date = $this->FormatDate($data['date']);
			}else{
				$date='';
			}
			$update = array(
				'id_categoria'=>$data['category'],
				'id_marca'=>$data['brand'],
				'nombre'=>$data['name'],
				'fecha_caducidad'=>$date, 
				'precio'=>$data['price'], 
				'detalle'=>$data['detail'], 
				'codigo_barra'=>$codigo
			);
            $where = array('id_producto'=>$data['id_product'], 'id_entidad'=>$data['entidad']);
            $this->db->where($where);
            $this->db->update('pr_producto', $update);
            if($this->db->affected_rows() > 0){
                return array('message'=>'PRODUCTO ACTUALIZADO', 'response'=>true);
            }else{
                return array('message'=>'ERROR AL TRATAR DE ACTUALIZAR EL PRODUCTO', 'response'=>false);
            }
        }catch(Exception $ex){
            return $ex;
        }
	}

	public function ProductTable($data){
		try{
			$where = array('p.id_entidad'=>$data['entidad']);
			$select = 'p.id_producto, ifnull(m.detalle, "sin marca") as marca, p.id_marca, ifnull(c.detalle, "sin categoria") as categoria, p.id_categoria, 
			p.nombre, p.precio, p.codigo_barra, p.id_estado, p.fecha_caducidad, p.detalle';
            $this->db->select($select);
            $this->db->like('p.nombre', $data['search']);
			$this->db->from('pr_producto as p', $data['page'], $data['records']);
			$this->db->join('pr_categoria as c', 'c.id_categoria = p.id_categoria', 'left');
			$this->db->join('pr_marca as m', 'm.id_marca = p.id_marca', 'left');
            $this->db->where($where);
			$query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
		}
	}

	public function GetTotalProduct($data){
        try{
            $where = array('id_entidad'=>$data['entidad']);
            $this->db->select('count(id_producto) as total');
            $this->db->like('nombre', $data['search']);
            $this->db->from('pr_producto');
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()[0]->total > 0){
                return $query->result()[0]->total;
            }else{
                return 0;
            }
        }catch(Exception $ex){
            return $ex;
        }
        
    }
	
	public function ChangeStateProduct($data){
        try{
            $update = array(
				'id_estado'=>$data['state'],
			);
            $where = array('id_producto'=>$data['id_product'], 'id_entidad'=>$data['entidad']);
            $this->db->where($where);
            $this->db->update('pr_producto', $update);
            if($this->db->affected_rows() > 0){
                return array('message'=>'ESTADO ACTUALIZADO', 'response'=>true);
            }else{
                return array('message'=>'ERROR AL TRATAR DE ACTUALIZAR EL ESTADO', 'response'=>false);
            }
        }catch(Exception $ex){
            return $ex;
        }
	}

	private function FormatDate($date){
		$sepDate = explode('/', $date);
		return $sepDate[2].'-'.$sepDate[1].'-'.$sepDate[0];
	}

	public function GetBrands($entidad){
		try{
            $where = array('id_entidad'=>$entidad);
            $this->db->select('id_marca, detalle');
            $this->db->from('pr_marca');
            $this->db->where($where);
            $query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
		}
	}

	public function GetAllCategory($entidad){
        $this->db->select('id_categoria, detalle, padre');
        $this->db->from('pr_categoria');
        $this->db->where('id_entidad', $entidad);
        $query = $this->db->get();
        if(!empty($query->result())){
            $new = array();
            foreach ($query->result() as $a){
                $new[$a->padre][] = array('id'=>$a->id_categoria, 'parentid'=>$a->padre, 'name'=>$a->detalle);
            }
            $tree = $this->createTree($new, $new[0]);
            return $tree;
        }else{
            return false;
        }
    }

    private function createTree(&$list, $parent){
		$tree = array();
		foreach ($parent as $k=>$l){
            if(isset($list[$l['id']])){
                $l['children'] = $this->createTree($list, $list[$l['id']]);
            }
            $tree[] = $l;
        } 
        return $tree;
    }
}
?>