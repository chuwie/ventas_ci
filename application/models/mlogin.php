<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mlogin extends CI_Model{

	function __construct(){
		parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->database();
    }

    public function IsValid($data){
        try{
            /* Validar existencia del email*/
            $this->db->select('id_entidad');
            $this->db->from('cl_entidad');
            $this->db->where('email', $data['email']);
            $query = $this->db->get();
            if(!empty($query->result())){
                $id_entidad = $query->result()[0]->id_entidad;
                /* Validar usuario y contraseña con los datos de la entidad*/
                $where = array('id_entidad'=>$id_entidad, 'user'=>$data['username'], 'password'=>md5($data['password']));
                $this->db->select('count(id_entidad) as total');
                $this->db->from('cl_entidad');
                $this->db->where($where);
                $query = $this->db->get();
                if($query->result()[0]->total > 0){
                    //Retornar valido y tipo user = 0 para crear session de entidad, obtiene las sucursales relacionadas
                    return array('message'=>'venta', 'entidad'=>$id_entidad, 'tipo_user'=>0, 'sucursal'=>0, 'list'=>$this->LoadBranchoffice($id_entidad), 'response'=>true);
                }else{
                    /* Validar si el usuario y contraseña existen en la tabla perfiles, para loggear con el perfil de una sucursal */
                    $where = array('id_entidad'=>$id_entidad, 'user'=>$data['username'], 'password'=>md5($data['password']));
                    $this->db->select('id_sucursal, tipo');
                    $this->db->from('cl_perfil');
                    $this->db->where($where);
                    $query = $this->db->get();
                    if(!empty($query->result())){
                        $sucursal = $query->result()[0]->id_sucursal;
                        $tipo_user = $query->result()[0]->tipo;
                        //Retornar valido y tipo user para crear session de perfil de una sucursal
                        return array('message'=>'venta', 'entidad'=>$id_entidad, 'tipo_user'=>$tipo_user, 'sucursal'=>$sucursal, 'list'=>'', 'response'=>true);
                    }else{
                        return array('message'=>'Nombre de usuario o contrañesa incorrectos', 'response'=>false);
                    }
                }
            }else{
                return array('message'=>'No existe el email ingresado', 'response'=>false);
            }
        }catch(Exception $ex){
            return array('message'=>$ex, 'response'=>false);
        }
    }

    private function LoadBranchoffice($id_entidad){
        try{
            /* Obtiene las sucursales de una entidad */
            $where = array('id_entidad'=>$id_entidad, 'id_estado'=>'1');
            $this->db->select('id_sucursal, direccion');
            $this->db->from('cl_sucursal');
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()){
                return $query->result();
            }else{
                return '';
            }
        }catch(Exception $ex){
            return '';
        }
    }
}
?>