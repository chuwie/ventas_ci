<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mupdateuser extends CI_Model{

	public function __construct(){
		parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->database();
    }
    
    public function BranchofficeList($data){
        try{
			$where = array('id_entidad'=>$data['entidad'], 'id_estado'=>'1');
			$select = 'id_sucursal, direccion';
            $this->db->select($select);
			$this->db->from('cl_sucursal');
            $this->db->where($where);
            $query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
		}
    }

    public function ModifyUser($data){
        try{
            $validate = $this->ValidateNewPass($data);
            if(!$validate){
                $update = array(
                    'password'=>md5($data['newpass'])
                );
                $where = array('id_sucursal'=>$data['branchoffice'], 'tipo'=>$data['account'], 'id_entidad'=>$data['entidad']);
                $this->db->where($where);
                $this->db->update('cl_perfil', $update);
                if($this->db->affected_rows() > 0){
                    switch($data['account']){
                        case '1':
                                $info[]=array(
                                    'tipo'=>'ADMINISTRADOR SUCURSAL', 
                                    'usuario'=>'Admin', 
                                    'password'=>$data['newpass']
                                );
                            break;
                        case '2':
                                $info[]=array(
                                    'tipo'=>'VENTAS SUCURSAL', 
                                    'usuario'=>'Ventas', 
                                    'password'=>$data['newpass']
                                );
                            break;
                        case '3':
                                $info[]=array(
                                    'tipo'=>'CAJA SUCURSAL', 
                                    'usuario'=>'Caja', 
                                    'password'=>$data['newpass']
                                );
                            break;
                        case '4':
                                $info[]=array(
                                    'tipo'=>'BODEGA SUCURSAL', 
                                    'usuario'=>'Bodega', 
                                    'password'=>$data['newpass']
                                );
                            break;
                    }
                    if($this->mailer->SendMail($info, $data['email'], $data['name'], 'update')){
                        return array('message'=>'CONTRASEÑA ACTUALIZADA, EMAIL ENVIADO', 'response'=>true);
                    }else{
                        return array('message'=>'CONTRASEÑA ACTUALIZADA, PROBLEMA CON EL ENVIO DEL EMAIL', 'response'=>true);
                    }
                }else{
                    return array('message'=>'ERROR AL TRATAR DE ACTUALIZAR LA CONTRASEÑA', 'response'=>false);
                }
            }else{
                return array('message'=>'YA SE REGISTRO ESTA CONTRASEÑA', 'response'=>false);
            }
        }catch(Exception $ex){
            return $ex;
        }
    }

    private function ValidateNewPass($data){
        try{
			$where = array('password'=>md5($data['newpass']), 'id_sucursal'=>$data['branchoffice'], 'tipo'=>$data['account'], 'id_entidad'=>$data['entidad']);
			$select = 'count(id_perfil) as total';
            $this->db->select($select);
			$this->db->from('cl_perfil');
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()[0]->total > 0){
                return true;
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
		}
    }

}
?>