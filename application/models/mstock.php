<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mstock extends CI_Model{

	public function __construct(){
		parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->database();
    }
    
    public function ProductsList($data){
        try{
			$where = array('id_entidad'=>$data['entidad'], 'id_estado'=>'1');
			$select = 'id_producto, nombre';
            $this->db->select($select);
			$this->db->from('pr_producto');
            $this->db->where($where);
			$query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
		}
    }
    
    public function BranchofficeList($data){
        try{
            if(!empty($data['branchoffice']))
                $where = array('id_sucursal'=>$data['branchoffice'], 'id_entidad'=>$data['entidad'], 'id_estado'=>'1');
            else
                $where = array('id_entidad'=>$data['entidad'], 'id_estado'=>'1');

			$select = 'id_sucursal, direccion';
            $this->db->select($select);
			$this->db->from('cl_sucursal');
            $this->db->where($where);
            $query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
		}
    }
    
    public function InsertStockProduct($data){
        $validate = $this->ValidateStockProduct($data['product'], $data['branchoffice']);
        if(!$validate){
            $insert = array(
                'stock'=>$data['stock'],
                'id_sucursal'=>$data['branchoffice'],
                'id_producto'=>$data['product']
            );
            $this->db->insert('pr_producto_sucursal', $insert);
            if($this->db->affected_rows() != 1){
                return array('message'=>'ERROR AL TRATAR DE INGRESAR EL STOCK', 'response'=>false);
            }else{
                return array('message'=>'STOCK INGRESADO', 'response'=>true);
            }
        }else{
            return array('message'=>'YA HA INGRESADO ESTE PRODUCTO', 'response'=>false);
        }
    }

    private function ValidateStockProduct($product, $branchoffice){
        try{
            $where = array('id_producto'=>$product, 'id_sucursal'=>$branchoffice);
            $this->db->select('count(id_producto_sucursal) as total');
            $this->db->from('pr_producto_sucursal');
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()[0]->total > 0){
                return true;
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
        }
    }

    public function ModifyStockProduct($data){
        try{
			$update = array(
				'stock'=>$data['stock']
			);
            $where = array('id_producto_sucursal'=>$data['id'], 'id_producto'=>$data['product'], 'id_sucursal'=>$data['branchoffice']);
            $this->db->where($where);
            $this->db->update('pr_producto_sucursal', $update);
            if($this->db->affected_rows() > 0){
                return array('message'=>'STOCK ACTUALIZADO', 'response'=>true);
            }else{
                return array('message'=>'ERROR AL TRATAR DE ACTUALIZAR EL STOCK', 'response'=>false);
            }
        }catch(Exception $ex){
            return $ex;
        }
    }

    public function StockProductTable($data){
		try{
			$where = array('p.id_entidad'=>$data['entidad']);
			$select = 'ps.id_producto_sucursal, p.id_producto, s.id_sucursal, p.nombre, s.direccion, ps.stock';
            $this->db->select($select);
			$this->db->from('pr_producto_sucursal ps', $data['page'], $data['records']);
			$this->db->join('pr_producto p', '(p.id_producto = ps.id_producto and p.id_entidad = '.$data['entidad'].')', 'inner');
            $this->db->join('cl_sucursal s', '(s.id_sucursal = ps.id_sucursal and s.id_entidad = '.$data['entidad'].')', 'inner');
            $this->db->like('UPPER(p.nombre)', strtoupper($data['search']));
            $this->db->or_like('UPPER(s.direccion)', strtoupper($data['search']));
            $this->db->order_by('p.id_producto', 'ASC');
            $this->db->where($where);
            $query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
		}
    }

    public function GetTotalStockProduct($data){
        try{
            $where = array('p.id_entidad'=>$data['entidad']);
            $this->db->select('count(ps.id_producto_sucursal) as total');
            $this->db->from('pr_producto_sucursal ps');
			$this->db->join('pr_producto p', '(p.id_producto = ps.id_producto and p.id_entidad = '.$data['entidad'].')', 'inner');
            $this->db->join('cl_sucursal s', '(s.id_sucursal = ps.id_sucursal and s.id_entidad = '.$data['entidad'].')', 'inner');
            $this->db->like('p.nombre', $data['search']);
            $this->db->or_like('s.direccion', $data['search']);
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()[0]->total > 0){
                return $query->result()[0]->total;
            }else{
                return 0;
            }
        }catch(Exception $ex){
            return $ex;
        }
        
    }
}
?>