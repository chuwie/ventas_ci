<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mbrands extends CI_Model{

	function __construct(){
		parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->database();
    }

    public function InsertBrands($data){
        $validate = $this->ValidateBrands($data['name'], $data['entidad']);
        if(!$validate){
            $this->db->insert('pr_marca', array('detalle'=>$data['name'], 'id_entidad'=>$data['entidad']));
            if($this->db->affected_rows() != 1){
                return array('message'=>'ERROR AL TRATAR DE REGISTRAR LA MARCA', 'response'=>false);
            }else{
                return array('message'=>'MARCA REGISTRADA', 'response'=>true);
            }
        }else{
            return array('message'=>'YA HA REGISTRADO ESTA MARCA', 'response'=>false);
        }
    }

    public function ModifyBrands($data){
        try{
            $id_entidad = $data['entidad'];
            $info = array('detalle'=>$data['name']);
            $where = array('id_marca'=>$data['id_brands'], 'id_entidad'=>$id_entidad);
            $this->db->where($where);
            $this->db->update('pr_marca', $info);
            if($this->db->affected_rows() > 0){
                return array('message'=>'MARCA ACTUALIZADA', 'response'=>true);
            }else{
                return array('message'=>'ERROR AL TRATAR DE ACTUALIZAR LA MARCA', 'response'=>false);
            }
        }catch(Exception $ex){
            return $ex;
        }
    }

    private function ValidateBrands($name, $entidad){
        try{
            $where = array('detalle'=>$name, 'id_entidad'=>$entidad);
            $this->db->select('count(id_marca) as total');
            $this->db->from('pr_marca');
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()[0]->total > 0){
                return true;
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
        }
    }

    public function GetAllCategory(){
        try{
            $this->db->select('id_categoria, detalle, padre');
            $this->db->from('pr_categoria');
            $query = $this->db->get();
            if(!empty($query->result())){
                $new = array();
                foreach ($query->result() as $a){
                    $new[$a->padre][] = array('id'=>$a->id_categoria, 'parentid'=>$a->padre, 'name'=>$a->detalle);
                }
                $tree = $this->createTree($new, $new[0]);
                return $tree;
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
        }
    }

    private function createTree(&$list, $parent){
		$tree = array();
		foreach ($parent as $k=>$l){
            if(isset($list[$l['id']])){
                $l['children'] = $this->createTree($list, $list[$l['id']]);
            }
            $tree[] = $l;
        } 
        return $tree;
    }
    
    public function GetBrands($data){
        try{
            $where = array('id_entidad'=>$data['entidad']);
            $this->db->select('id_marca, detalle, id_estado');
            $this->db->like('detalle', $data['search']);
            $this->db->from('pr_marca', $data['page'], $data['records']);
            $this->db->where($where);
            $query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
        }
    }

    public function GetTotalBrands($data){
        try{
            $where = array('id_entidad'=>$data['entidad']);
            $this->db->select('count(id_marca) as total');
            $this->db->like('detalle', $data['search']);
            $this->db->from('pr_marca');
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()[0]->total > 0){
                return $query->result()[0]->total;
            }else{
                return 0;
            }
        }catch(Exception $ex){
            return $ex;
        }
        
    }

    public function ChangeState($data){
        try{
            $id_entidad = $data['entidad'];
            $info = array('id_estado'=>$data['state']);
            $where = array('id_marca'=>$data['id_brands'], 'id_entidad'=>$id_entidad);
            $this->db->where($where);
            $this->db->update('pr_marca', $info);
            if($this->db->affected_rows() > 0){
                return array('message'=>'ESTADO ACTUALIZADO', 'response'=>true);
            }else{
                return array('message'=>'ERROR AL TRATAR DE ACTUALIZAR EL ESTADO', 'response'=>false);
            }
        }catch(Exception $ex){
            return $ex;
        }
    }



}
?>