<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mbranchoffice extends CI_Model{

	function __construct(){
		parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->database();
	}

	function InsertBranchoffice($data){
		$validate = $this->ValidateBranchoffice($data['direction'], $data['entidad']);
        if(!$validate){
			$insert = array(
                'direccion'=>$data['direction'],
                'telefono'=>$data['phone'],
				'id_entidad'=>$data['entidad']
			);
            $this->db->insert('cl_sucursal', $insert);
            if($this->db->affected_rows() != 1){
                return array('message'=>'ERROR AL TRATAR DE INGRESAR LA SUCURSAL', 'response'=>false);
            }else{
                $id_sucursal = $this->db->insert_id();
                $cont = 4;
                for($i=0; $i<4; $i++){
                    $type=$i+1;
                    if($type==1){
                        $user='Admin';
                        $pass= $this->RandomPassword();
                        $name_type='ADMINISTRADOR SUCURSAL '.$data['direction'];
                    }else if($type==2){
                        $user='Ventas';
                        $pass= $this->RandomPassword();
                        $name_type='VENTAS SUCURSAL '.$data['direction'];
                    }else if($type==3){
                        $user='Caja';
                        $pass= $this->RandomPassword();
                        $name_type='CAJA SUCURSAL '.$data['direction'];
                    }if($type==4){
                        $user='Bodega';
                        $pass=$this->RandomPassword();
                        $name_type='BODEGA SUCURSAL '.$data['direction'];
                    }
                    $this->db->insert('cl_perfil', array('id_sucursal'=>$id_sucursal, 'tipo'=>$type, 'user'=>$user, 'password'=>md5($pass), 'id_entidad'=>$data['entidad']));
                    if($this->db->affected_rows() != 1){
                        return array('message'=>'SE HA REGISTRADO SU INFORMACION, PERO SE PRODUJO UN ERROR AL GENERAR SUS CREDENCIALES DE SUCURSAL '.$user.', POR FAVOR CONTACTE CON AL EMAIL moralesjaraf@gmail.com O AL (+56)940220374', 'response'=>false);
                    }else{
                        $info[]=array('tipo'=>$name_type, 'usuario'=>$user, 'password'=>$pass);
                    }
                }
                if($this->mailer->SendMail($info, $data['email'], $data['name'], 'create')){
                    return array('message'=>'SUCURSAL INGRESADA, PERO SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN A SU EMAIL, POR FAVOR CONTACTE CON AL EMAIL moralesjaraf@gmail.com O AL (+56)940220374', 'response'=>true);
                }else{
                    return array('message'=>'SUCURSAL INGRESADA', 'response'=>true);
                }
            }
        }else{
            return array('message'=>'YA HA INGRESADO ESTA SUCURSAL', 'response'=>false);
        }
	}

	private function ValidateBranchoffice($direction, $entidad){
        try{
            $where = array('direccion'=>$direction, 'id_entidad'=>$entidad);
            $this->db->select('count(id_sucursal) as total');
            $this->db->from('cl_sucursal');
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()[0]->total > 0){
                return true;
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
        }
    }

    function ModifyBranchoffice($data){
		try{
			$update = array(
				'direccion'=>$data['direction']
			);
            $where = array('id_sucursal'=>$data['id_branchoffice'], 'id_entidad'=>$data['entidad']);
            $this->db->where($where);
            $this->db->update('cl_sucursal', $update);
            if($this->db->affected_rows() > 0){
                return array('message'=>'SUCURSAL ACTUALIZADA', 'response'=>true);
            }else{
                return array('message'=>'ERROR AL TRATAR DE ACTUALIZAR LA SUCURSAL', 'response'=>false);
            }
        }catch(Exception $ex){
            return $ex;
        }
	}

    function BranchofficeTable($data){
		try{
			$where = array('id_entidad'=>$data['entidad']);
			$select = 'id_sucursal, direccion, id_estado';
            $this->db->select($select);
            $this->db->like('direccion', $data['search']);
			$this->db->from('cl_sucursal', $data['page'], $data['records']);
            $this->db->where($where);
			$query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            return $ex;
		}
	}
	
	public function GetTotalBranchoffice($data){
        try{
            $where = array('id_entidad'=>$data['entidad']);
            $this->db->select('count(id_sucursal) as total');
            $this->db->like('direccion', $data['search']);
            $this->db->from('cl_sucursal');
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()[0]->total > 0){
                return $query->result()[0]->total;
            }else{
                return 0;
            }
        }catch(Exception $ex){
            return $ex;
        }
    }
	
    public function ChangeStateBranchoffice($data){
		try{
            $update = array(
				'id_estado'=>$data['state'],
			);
            $where = array('id_sucursal'=>$data['branchoffice'], 'id_entidad'=>$data['entidad']);
            $this->db->where($where);
            $this->db->update('cl_sucursal', $update);
            if($this->db->affected_rows() > 0){
                return array('message'=>'ESTADO ACTUALIZADO', 'response'=>true);
            }else{
                return array('message'=>'ERROR AL TRATAR DE ACTUALIZAR EL ESTADO', 'response'=>false);
            }
        }catch(Exception $ex){
            return $ex;
        }
    }
    
    private function RandomPassword(){
        $caracteres = '0123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $caractereslong = strlen($caracteres);
        $clave = '';
        for($i = 0; $i < 24; $i++) {
            $clave .= $caracteres[rand(0, $caractereslong - 1)];
        }
        return $clave;
    }
}
?>