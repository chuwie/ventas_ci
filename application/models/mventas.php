<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mventas extends CI_Model{

	function __construct(){
		parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->database();
    }

    public function GetNumTicket($data){
        try{
            $where = array('id_entidad'=>$data['entidad']);
            $this->db->select('num_ticket');
            $this->db->from('cr_carro');
            $this->db->where($where);
            $this->db->order_by('num_ticket', 'desc');
            $query = $this->db->get();
            if(empty($query->result())){
                $insert = array(
                    'num_ticket'=>1,
                    'id_entidad'=>$data['entidad']
                );
                $this->db->insert('cr_carro', $insert);
                if($this->db->affected_rows() != 1){
                    return false;
                }else{
                    return '1|'.$this->db->insert_id();
                }
            }else{
                $num_ticket = intval($query->result()[0]->num_ticket) + 1;
                $insert = array(
                    'num_ticket'=>$num_ticket,
                    'id_entidad'=>$data['entidad']
                );
                $this->db->insert('cr_carro', $insert);
                if($this->db->affected_rows() != 1){
                    return false;
                }else{
                    return $num_ticket.'|'.$this->db->insert_id();
                }
            }
        }catch(Exception $ex){
            print_r($ex);
            die;
        }
    }

    public function DataUser($data){
        try {
            $url = 'https://siichile.herokuapp.com/consulta?rut='.$data['rut'];
            
            $ch = curl_init();
            if($ch===false) {
                throw new Exception('failed to initialize');
            }
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $content = curl_exec($ch);
            if($content===false) {
                throw new Exception(curl_error($ch), curl_errno($ch));
            }
            /* Process $content here */
            $info = json_decode($content);
            curl_close($ch);
            $isClient = $this->EsCliente($data);
            if($isClient){
                //Valida si la información registrada en la base de datos igual a la que esta en SII y la actualiza de ser necesario.
                $return = $this->GetDataUser($data)[0];
                $update = array();
                if($return->razon != $info->razon_social)
                    $update['razon'] = $info->razon_social;
                if($return->detalle != $info->actividades[0]->giro)
                    $update['detalle'] = $info->actividades[0]->giro;
                if($return->codigo != $info->actividades[0]->codigo)
                    $update['codigo'] = $info->actividades[0]->codigo;

                if(!empty($update)){
                    $where = array('rut'=>$data['rut'], 'id_entidad'=>$data['entidad']);
                    $this->db->where($where);
                    $this->db->update('cl_cliente', $update);
                }
                return $this->GetDataUser($data);
            }else{
                //rescata la informacion desde SII y la registra por primera vez en la tabla clientes de la BD
                $this->InsertGiro($info);
                if($this->InsertClient($info, $data)){
                    return $this->GetDataUser($data);
                }else{
                    return 'asdasd';
                }
            }
        }catch(Exception $ex){
            print_r($ex);
            trigger_error(sprintf('Curl failed with error #%d: %s',$e->getCode(), $e->getMessage()), E_USER_ERROR);
        }
    }

    private function EsCliente($data){
        try{
            $where = array('rut'=>$data['rut'], 'id_entidad'=>$data['entidad']);
            $this->db->select('count(id_cliente) as total');
            $this->db->from('cl_cliente');
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()[0]->total > 0){
                return true;
            }else{
                return false;
            }
        }catch(Exception $ex){
            print_r($ex);
            die;
        }
    }

    private function InsertGiro($info){
        try{
            $where = array('codigo'=>$info->actividades[0]->codigo, 'detalle'=>$info->actividades[0]->giro, 'categoria'=>$info->actividades[0]->categoria);
            $this->db->select('count(id_giro) as total');
            $this->db->from('cl_giro');
            $this->db->where($where);
            $query = $this->db->get();
            if($query->result()[0]->total == 0){
                $insert = array(
                    'codigo'=>$info->actividades[0]->codigo,
                    'detalle'=>$info->actividades[0]->giro,
                    'categoria'=>$info->actividades[0]->categoria
                );
                $this->db->insert('cl_giro', $insert);
                if($this->db->affected_rows() != 1){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }catch(Exception $ex){
            print_r($ex);
            die;
        }
    }

    private function InsertClient($info, $data){
        try{
            $insert = array(
                'rut'=>$info->rut,
                'razon'=>$info->razon_social,
                'direccion'=>'',
                'codigo'=>$info->actividades[0]->codigo,
                'id_entidad'=>$data['entidad']
            );
            $this->db->insert('cl_cliente', $insert);
            if($this->db->affected_rows() != 1){
                return false;
            }else{
                return true;
            }
        }catch(Exception $ex){
            print_r($ex);
            die;
        }
    }

    private function GetDataUser($data){
        try{
            $where = array('c.rut'=>$data['rut'], 'c.id_entidad'=>$data['entidad']);
            $this->db->select('c.rut, c.razon, c.direccion, g.detalle, g.codigo');
            $this->db->from('cl_cliente c');
            $this->db->join('cl_giro g', 'g.codigo = c.codigo', 'inner');
            $this->db->where($where);
            $query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            print_r($ex);
            die;
        }
    }

    private function getPuntosRut($rut){
        $rutTmp = explode("-", $rut);
        return number_format($rutTmp[0], 0, "", ".") . '-' . $rutTmp[1];
    }

    public function ListProduct($data){
        try{
            if(!empty($data['sucursal']))
                $where = array('ps.id_sucursal'=>$data['sucursal'], 'id_entidad'=>$data['entidad'], 'id_estado'=>1);
            else
                $where = array('id_entidad'=>$data['entidad'], 'id_estado'=>1);
            
            $this->db->select('p.id_producto, p.nombre');
            $this->db->from('pr_producto p');
            $this->db->join('pr_producto_sucursal ps', 'ps.id_producto = p.id_producto', 'inner');
            $this->db->where($where);
            $this->db->group_by("p.id_producto");
            $query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            print_r($ex);
            die;
        }
    }

    public function GetInfoProducts($data){
        try{
            $where = array('id_producto'=>$data['id_producto']);
            $this->db->select('id_producto, nombre, precio');
            $this->db->from('pr_producto');
            $this->db->where($where);
            $query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            print_r($ex);
            die;
        }
    }

    public function GetIdClient($rut){
        try{
            $where = array('rut'=>$rut);
            $this->db->select('id_cliente');
            $this->db->from('cl_cliente');
            $this->db->where($where);
            $query = $this->db->get();
            if(!empty($query->result())){
                return $query->result();
            }else{
                return false;
            }
        }catch(Exception $ex){
            print_r($ex);
            die;
        }
    }

    public function CreateTicket($data){
        try{
            $update = array(
                'id_cliente'=>$data['id_cliente'],
                'tipo_cliente'=>$data['tipo_cliente'],
                'document'=>$data['document'],
                'pago'=>$data['pago'],
                'total_venta'=>$data['total_venta']
            );
            $where = array('id_carro'=>$data['id_carro'], 'id_entidad'=>$data['entidad']);
            $this->db->where($where);
            $this->db->update('cr_carro', $update);
            if($this->db->affected_rows() > 0){
                $prods = $data['prods'];
                foreach($prods as $prod){
                    $insert = array(
                        'id_carro'=>$data['id_carro'],
                        'id_producto'=>$prod['id_producto'],
                        'cantidad'=>$prod['cantidad'],
                        'descuento'=>$prod['descuento'],
                        'monto'=>$prod['monto'],
                        'total'=>$prod['total']
                    );
                    $this->db->insert('cr_detalle_carro', $insert);
                }
                return true;
            }else{
                return false;
            }
        }catch(Exception $ex){
            print_r($ex);
            die;
        }
    }
}
?>