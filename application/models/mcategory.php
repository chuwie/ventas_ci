<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mcategory extends CI_Model{

	function __construct(){
		parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->database();
    }

    public function InsertCategory($data){
        $validate = $this->ValidateFather($data['name'], $data['entidad']);
        if(!$validate){
            $this->db->insert('pr_categoria', array('detalle'=>$data['name'], 'padre'=>$data['category'], 'id_entidad'=>$data['entidad']));
            if($this->db->affected_rows() != 1){
                return array('message'=>'ERROR AL TRATAR DE REGISTRAR LA CATEGORIA', 'response'=>false);
            }else{
                return array('message'=>'CATEGORIA REGISTRADA', 'response'=>true);
            }
        }else{
            return array('message'=>'YA HA REGISTRADO ESTA CATEGORIA PADRE', 'response'=>false);
        }
    }

    public function Modifycategory($data){
        $id_entidad = $data['entidad'];
        $info = array('detalle'=>$data['name'], 'padre'=>$data['category']);
        $where = array('id_categoria'=>$data['id_category'], 'id_entidad'=>$id_entidad);
        $this->db->where($where);
        $this->db->update('pr_categoria', $info);
        if($this->db->affected_rows() > 0){
            return array('message'=>'CATEGORIA ACTAULIZADA', 'response'=>true);
        }else{
            return array('message'=>'ERROR AL TRATAR DE ACTUALIZAR LA CATEGORIA', 'response'=>false);
        }
    }

    private function ValidateFather($name, $entidad){
        $where = array('detalle'=>$name, 'id_entidad'=>$entidad, 'padre'=>0);
        $this->db->select('count(id_categoria) as total');
        $this->db->from('pr_categoria');
        $this->db->where($where);
        $query = $this->db->get();
        if($query->result()[0]->total > 0){
            return true;
        }else{
            return false;
        }
    }

    public function GetAllCategory($entidad){
        $this->db->select('id_categoria, detalle, padre');
        $this->db->from('pr_categoria');
        $this->db->where('id_entidad', $entidad);
        $query = $this->db->get();
        if(!empty($query->result())){
            $new = array();
            foreach ($query->result() as $a){
                $new[$a->padre][] = array('id'=>$a->id_categoria, 'parentid'=>$a->padre, 'name'=>$a->detalle);
            }
            $tree = $this->createTree($new, $new[0]);
            return $tree;
        }else{
            return false;
        }
    }

    private function createTree(&$list, $parent){
		$tree = array();
		foreach ($parent as $k=>$l){
            if(isset($list[$l['id']])){
                $l['children'] = $this->createTree($list, $list[$l['id']]);
            }
            $tree[] = $l;
        } 
        return $tree;
    }
    
}
?>