<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class mregister extends CI_Model{

    private $sess_code;
    private $ip = '200.74.101.155';
    private $meta;

	function __construct(){
		parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->database();
        $this->sess_code = $this->session->userdata('code');
        // $this->ip = $this->GetIpClient();
        $this->meta = unserialize($this->GetInfoIP($this->ip));
    }

    public function CodeValidate($code){
        $where = array('codigo = '=>$code, 'id_estado = '=>1);
        $this->db->select('count(id_codigo) as total');
        $this->db->from('reg_codigos');
        $this->db->where($where);
        $query = $this->db->get();
        if($query->result()[0]->total > 0){
            $this->session->set_userdata('code', $code);
            return true;
        }else{
            return false;
        }
    }
    
    public function RegisterEntity($data){
        $validate = $this->ValidateUser($data['email'], $data['company'], $data['rut']);
        if(!$validate){
            $this->db->insert('cl_entidad', array('nombre'=>$data['name'], 'apellidos'=>$data['surnames'], 'empresa'=>$data['company'], 'rut_empresa'=>$data['rut'], 'email'=>$data['email'], 'telefono'=>$data['personalphone'], 'user'=>$data['username'], 'password'=>md5($data['password'])));
            if($this->db->affected_rows() != 1){
                return array('message'=>'ERROR AL TRATAR DE REGISTRAR', 'response'=>false);
            }else{
                $id_entidad = $this->db->insert_id();
                $this->db->insert('cl_sucursal', array('id_entidad'=>$id_entidad, 'direccion'=>$data['direction'], 'telefono'=>$data['phone']));
                if($this->db->affected_rows() != 1){
                    $this->db->where('id_entidad', $id_entidad);
                    $this->db->delete('cl_entidad');
                    return array('message'=>'ERROR AL TRATAR DE REGISTRAR LA SUCURSAL', 'response'=>false);
                }else{
                    //GENERAR LOS USUARIO CORRESPONDIENTES A LA SUSCURSAL Y ENVIAR EL CORREO ELECTRONICO CON LA INFORMACION.
                    $id_sucursal = $this->db->insert_id();
                    $cont = 4;
                    for($i=0; $i<4; $i++){
                        $type=$i+1;
                        if($type==1){
                            $user='Admin';
                            $pass= $this->RandomPassword();
                            $name_type='ADMINISTRADOR SUCURSAL '.$data['direction'];
                        }else if($type==2){
                            $user='Ventas';
                            $pass= $this->RandomPassword();
                            $name_type='VENTAS SUCURSAL '.$data['direction'];
                        }else if($type==3){
                            $user='Caja';
                            $pass= $this->RandomPassword();
                            $name_type='CAJA SUCURSAL '.$data['direction'];
                        }if($type==4){
                            $user='Bodega';
                            $pass=$this->RandomPassword();
                            $name_type='BODEGA SUCURSAL '.$data['direction'];
                        }
                        $this->db->insert('cl_perfil', array('id_sucursal'=>$id_sucursal, 'tipo'=>$type, 'user'=>$user, 'password'=>md5($pass), 'id_entidad'=>$id_entidad));
                        if($this->db->affected_rows() != 1){
                            return array('message'=>'SE HA REGISTRADO SU INFORMACION, PERO SE PRODUJO UN ERROR AL GENERAR SUS CREDENCIALES DE SUCURSAL '.$user.', POR FAVOR CONTACTE CON AL EMAIL moralesjaraf@gmail.com O AL (+56)940220374', 'response'=>false);
                        }else{
                            $info[]=array('tipo'=>$name_type, 'usuario'=>$user, 'password'=>$pass);
                        }
                    }
                    if(count($info)>0){
                        // $sent = $this->SendMail($info, $data['email'], $data['name']);
                        if($this->mailer->SendMail($info, $data['email'], $data['name'], 'new')){
                            $data = array(
                                'countryCode'=>$this->meta['countryCode'],
                                'regionName'=>$this->meta['regionName'],
                                'city'=>$this->meta['city'],
                                'timezone'=>$this->meta['timezone'],
                                'country'=>$this->meta['country'],
                                'lat'=>$this->meta['lat'],
                                'lon'=>$this->meta['lon'],
                                'as'=>$this->meta['as'],
                                'isp'=>$this->meta['isp'],
                                'query'=>$this->meta['query'],
                                'region'=>$this->meta['region'],
                                'org'=>$this->meta['org'],
                                'id_estado'=>'3',
                                'id_entidad'=>$id_entidad
                            );
                            $this->db->where('codigo', $this->sess_code);
                            $this->db->update('reg_codigos', $data);
                            $this->session->unset_userdata('code');
			                $this->session->sess_destroy();
                            return array('message'=>'REVISE SU BANDEJA DE ENTRADA O CORREO NO DESEADO, SE LE HA ENVIADO LA INFORMACIÓN PARA COMENZAR A OPERAR CON NUESTRO SISTEMA, MUCHAS GRACIAS', 'response'=>true);
                        }else{
                            return array('message'=>'PROBLEMAS AL ENVIAR EL CORREO CON SU INFORMACIÓN, POR FAVOR CONTACTE CON AL EMAIL moralesjaraf@gmail.com O AL (+56)940220374', 'response'=>false);
                        }
                    }
                }
            }
        }else{
            return array('message'=>'YA HA REGISTRADO SU NEGOCIO', 'response'=>false);
        }
    }

    public function ValidateUser($email, $company, $rut){
        $this->db->select('count(id_entidad) as total');
        $this->db->from('cl_entidad');
        $this->db->where('email', $email);
        $this->db->or_where('empresa', $company);
        $this->db->or_where('rut_empresa', $rut);
        $query = $this->db->get();
        if($query->result()[0]->total > 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function RandomPassword(){
        $caracteres = '0123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $caractereslong = strlen($caracteres);
        $clave = '';
        for($i = 0; $i < 24; $i++) {
            $clave .= $caracteres[rand(0, $caractereslong - 1)];
        }
        return $clave;
    }

    // public function SendMail($info, $email_to, $name){
    //     header('Content-Type: application/json');

    //     require_once APPPATH . 'libraries/PHPMailer/src/Exception.php';
    //     require_once APPPATH . 'libraries/PHPMailer/src/PHPMailer.php';
    //     require_once APPPATH . 'libraries/PHPMailer/src/SMTP.php';

    //     $mail = new PHPMailer(true);
    //     try {
    //         $body = 'Bienvenido a nuestra plataforma, te entregamos las credenciales asociadas a tu sucursal: </br></br>';
    //         foreach($info as $value){
    //             $body .= '<b>Tipo: </b>'.$value['tipo'].'</br>';
    //             $body .= '<b>Usuario: </b>'.$value['usuario'].'</br>';
    //             $body .= '<b>Contraseña: </b>'.$value['password'].'</br></br>';
    //         }
    //         $body .= 'También le otorgamos el link de su "Panel de Administrador".</br></br>';
    //         $body .= 'Link: <a href="http://localhost/ventas_ci/login">http://localhost/ventas_ci/login</a> </br></br>';
    //         $body .= 'Para más información o para realizar consultas, escribanos a moralesjaraf@gmail.com o llame al (+56) 940220374. </br>';
    //         $body .= 'Gracias por su registrarse.';

    //         //Server settings
    //         // $mail->SMTPDebug = 2;                                    // Enable verbose debug output
    //         $mail->isSMTP();                                            // Set mailer to use SMTP
    //         $mail->Host       = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
    //         $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    //         $mail->Username   = 'moralesjaraf@gmail.com';               // SMTP username
    //         $mail->Password   = 'hrjzuntkegachyxg';                     // SMTP password
    //         $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
    //         $mail->Port       = 587;                                    // TCP port to connect to

    //         //Recipients
    //         $mail->setFrom('moralesjaraf@gmail.com', 'Felipe Morales');
    //         $mail->addAddress($email_to, $name);                        // Add a recipient
    //         // $mail->addAddress('ellen@example.com');                     // Name is optional
    //         // $mail->addReplyTo('info@example.com', 'Information');
    //         // $mail->addCC('cc@example.com');
    //         // $mail->addBCC('bcc@example.com');

    //         // Attachments
    //         // $mail->addAttachment('/var/tmp/file.tar.gz');               // Add attachments
    //         // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');          // Optional name

    //         // Content
    //         $mail->isHTML(true);                                           // Set email format to HTML
    //         $mail->Subject = 'Bienvenido - Credenciales';
    //         $mail->Body    = $body;
    //         // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
    //         $mail->CharSet = 'UTF-8';
            
    //         if($mail->send()){
    //             return true;
    //         }else{
    //             return false;
    //         }
    //     }catch(Exception $e){
    //         echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    //         return false;
    //     }
    // }

    private function GetIpClient(){
        if (getenv('HTTP_CLIENT_IP')) {
            $ip = getenv('HTTP_CLIENT_IP');
        }elseif(getenv('HTTP_X_FORWARDED_FOR')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        }elseif(getenv('HTTP_X_FORWARDED')) {
            $ip = getenv('HTTP_X_FORWARDED');
        }elseif(getenv('HTTP_FORWARDED_FOR')) {
            $ip = getenv('HTTP_FORWARDED_FOR');
        }elseif(getenv('HTTP_FORWARDED')) {
            $ip = getenv('HTTP_FORWARDED');
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    private function GetInfoIP($ip){
        // $url = 'http://www.geoplugin.net/php.gp?ip='.$ip;
        $url = 'http://ip-api.com/php/'.$ip;
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_TIMEOUT, 15);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        $meta = curl_exec($c);
        $status = curl_getinfo($c, CURLINFO_HTTP_CODE);
        curl_close($c);

        return $meta;
    }
}
?>