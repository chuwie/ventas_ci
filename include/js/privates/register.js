var is_valid=false, mail_valid=false;
$(document).ready(function(){ 
    RestrictFields();
    LoadCodes();
    SendData();
});

function RestrictFields(){
    $('#name').keypress(function(event){ return soloNumerosLetras(event); });
    $('#surnames').keypress(function(event){ return soloNumerosLetras(event); });
    $('#personalphone').keypress(function(event){ return soloNumeros(event); });
    $('#username').keypress(function(event){ return soloNumerosLetras(event); });
    
    $('#email').blur(function(){
        if($(this).validationEngine('validate')){
            $(this).css({'border-color': '#4f5467'});
        }else{
            $(this).css({'border-color': 'red'});
        }
    });

    $('#personalphone').blur(function(){
        if($(this).validationEngine('validate')){
            $(this).css({'border-color': '#4f5467'});
        }else{
            $(this).css({'border-color': 'red'});
        }
    });

    $('#password').blur(function(){
        if($('#confirm').validationEngine('validate')){
            $(this).css({'border-color': '#4f5467'});
            $('#confirm').css({'border-color': '#4f5467'});
        }else{
            $(this).css({'border-color': 'red'});
            $('#confirm').css({'border-color': 'red'});
        }
    });

    $('#confirm').blur(function(){
        if($(this).validationEngine('validate')){
            $(this).css({'border-color': '#4f5467'});
            $('#password').css({'border-color': '#4f5467'});
        }else{
            $(this).css({'border-color': 'red'});
            $('#password').css({'border-color': 'red'});
        }
    });
    
    $('#rut').blur(function(){ 
        is_valid = checkRut($(this).val()); 
        $(this).val(formateaRut($(this).val()));
    });

    $('#company').keypress(function(event){ return soloNumerosLetras(event); });
    $('#phone').keypress(function(event){ return soloNumeros(event); });
}

function SendData(){
    $('.btn-register').click(function(){
        if(is_valid){
            $('.btn-register').prop('disabled', true);
            var form = $('.form-register');
            if(form.validationEngine('validate')){
                $.ajax({
                    data: form.serialize(),
                    url: 'register',
                    type: 'POST',
                    success: function(result)
                    {
                        var msn = result;
                        if(msn['response']){
                            SuccessMessage(msn['message']);
                            $('.btn-register').prop('disabled', true);
                        }else{
                            ErrorMessage(msn['message']);
                            $('.btn-register').prop('disabled', false);
                        }
                    }
                });
            }else{
                $('.btn-register').prop('disabled', false);
            }
        }
        else{
            ErrorMessage("RUT Inválido");
            $('.btn-register').prop('disabled', false);
        }
    });
}

function LoadCodes(){
    codes.sort(OrderByPrefijo);
    $.each(codes, function(index, value){
        var option = '<option value="'+value.prefijo+'">'+value.prefijo+'</option>';
        $('#code1').append(option);
        $('#code2').append(option);

        $('#code1').val(56);
        $('#code2').val(56);

    });
}

function OrderByPrefijo(a, b) {
    // console.log();
    if (a['prefijo'] === b['prefijo']) {
        return 0;
    }
    else {
        return (a['prefijo'] < b['prefijo']) ? -1 : 1;
    }
}