var ventas, form_ventas, aProd=[];

$(document).ready(function(){
    GetDataProduct();
    ChangeTypeDocument();
    ventas = $('#tventas').DataTable({
        "language":{
            "decimal":        "",
            "emptyTable":     "No hay datos",
            "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
            "infoFiltered":   "(Filtro de _MAX_ total registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "zeroRecords":    "No se encontraron coincidencias",
            "paginate": {
                "first":      "Primero",
                "last":       "Ultimo",
                "next":       "Próximo",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar orden de columna ascendente",
                "sortDescending": ": Activar orden de columna desendente"
            }
        },  
        "scrollY": "500px",
        "scrollCollapse": true,
        "paging": false,
        "ordering": false
    });
    $('#date').val(new Date($.now()).toLocaleDateString());

    SendTicket();

    $('#rut').keypress(function(event){
        soloRut(event);
    });
});

function HidenProps(){
    $('#form-vantas').validationEngine('hideAll');
}

function GetInfoCliente(rut){
    $.ajax({
        data: {rut: rut},
        url: 'inforut',
        type: 'POST',
        success: function(result){
            if(result != ''){
                $.each(result, function(index, value){
                    $('#razon').val(value.razon);
                    // $('#giro').append('<option value="'+value.codigo+'">'+value.detalle+'</option>');
                    $('#giro').val(value.detalle);
                    // $('#select2-giro-container').text(value.detalle);
                });
            }
        }
    });
    // jQuery.getJSON('https://siichile.herokuapp.com/consulta', {rut: rut}, function(result){
    //     console.log(result);
    //     if(result.actividades!=null){
            
    //         $('#razon').val(result.razon_social);
    //         $('#giro').append('<option value="'+result.actividades[0].codigo+'">'+result.actividades[0].giro+'</option>');
    //         $('#giro').val(result.actividades[0].codigo);
    //         $('#select2-giro-container').text(result.actividades[0].giro);
    //     }
    // });
}

function GetDataProduct(){
    $('#producto').unbind('change');
    $('#producto').change(function(){
        if($(this).val()!=''){
            if(!FindMutiarray(aProd, $(this).val(), 'id_producto')){
                $.ajax({
                    data: {id_producto: $(this).val()},
                    url: 'dataproduct',
                    type: 'POST',
                    success: function(result){
                        // aProd.push(result[0].id_producto);
                        aProd.push({
                            'id_producto': result[0].id_producto,
                            'cantidad': 1,
                            'descuento': 0,
                        });
                        ventas.row.add([
                            result[0].id_producto,
                            '<input type="text" class="form-control numeric" onInput="GetTotal($(this).val(), '+result[0].precio+', '+result[0].id_producto+')" value="1" min="1"/>',
                            result[0].nombre,
                            CreateSelectDiscounts(result[0].id_producto, result[0].precio),
                            '$ '+result[0].precio,
                            '$ <span id="total'+result[0].id_producto+'">'+result[0].precio+'</span>',
                            '<button type="button" class="btn btn-danger btn-delete" data-delete="'+result[0].id_producto+'"><span class="fas fa-trash"></span></button>'
                        ]).draw().node();
                        
                        $('.numeric').unbind('keyress');
                        $('.numeric').keypress(function(event){
                            soloNumeros(event);
                        });

                        $('.numeric').unbind('blur');
                        $('.numeric').blur(function(event){
                            if($(this).val()=='' || $(this).val()==0){
                                $(this).val(1);
                                GetTotal(1, result[0].precio, result[0].id_producto);
                            }
                        });
                        
                        CalculateTotal();

                        DeleteProduct(result[0].precio);
                    }
                });
                
            }else{
                ErrorMessage('EL PRODUCTO YA SE ENCUENTRA AGREGADO');
            }
        }
    });
}

function GetTotal(cant, price, id){
    var index = FindIndexMutiarray(aProd, id, 'id_producto');
    console.log(index);
    if(index!=false || index==0){
        var total = cant * price;
        aProd[index]['cantidad'] = cant;
        if($('#desc'+id).val()>0){
            total = total - ($('#desc'+id).val() * total) / 100;
        }
        $('#total'+id).text(Math.ceil(total));
        CalculateTotal();
    }
}

function CreateSelectDiscounts(id, price){
    var html='<select class="form-control" onChange="ChangeDiscounts($(this).val(), '+id+', '+price+')" id="desc'+id+'">'+
        '<option value="0">0%</option>'+
        '<option value="1">1%</option>'+
        '<option value="3">3%</option>'+
        '<option value="5">5%</option>'+
        '<option value="10">10%</option>'+
        '<option value="15">15%</option>'+
    '</select>';
    return html;
}

function DeleteProduct(price){
    $('.btn-delete').unbind('click');
    $('.btn-delete').click(function(){
        var id = $(this).attr('data-delete');
        var index = FindIndexMutiarray(aProd, id, 'id_producto');
        if(index!=false || index==0){
            aProd.splice(index, 1);
        }
        ventas.row(index).remove().draw(false);
        CalculateTotal();
    });
}

function ChangeDiscounts(val, id, price){
    var index = FindIndexMutiarray(aProd, id, 'id_producto');
    if(index!=false || index==0){
        var cant = aProd[index]['cantidad'];
        var total = cant * price;
        total = total - (val * total) / 100;
        aProd[index]['descuento'] = val;
        $('#total'+id).text(Math.ceil(total));
        CalculateTotal();
    }
}

function ChangeTypeDocument(){
    $('#document').unbind('change');
    $('#document').change(function(){
        var type = $(this).val();
        if(type=='boleta'){
            $('#rut').removeClass('validate[required]');
            $('#giro').removeClass('validate[required]');
            $('#razon').removeClass('validate[required]');
            $('#diraction').removeClass('validate[required]');

            $('#rut').prop('disabled', true);
            $('#giro').prop('disabled', true);
            $('#razon').prop('disabled', true);
            $('#diraction').prop('disabled', true);

        }else if(type=='factura'){
            $('#rut').addClass('validate[required]');
            $('#giro').addClass('validate[required]');
            $('#razon').addClass('validate[required]');
            $('#diraction').addClass('validate[required]');

            $('#rut').prop('disabled', false);
            $('#giro').prop('disabled', false);
            $('#razon').prop('disabled', false);
            $('#diraction').prop('disabled', false);

            $('#rut').blur(function(){
                if($(this).val()!=''){
                    GetInfoCliente($(this).val());
                    // console.log('18.572.197-3');
                    $('#giro').validationEngine('hide');
                }else{
                    $(this).validationEngine('showPrompt', 'Rut no puede ser vacio para la busqueda automática');
                    setTimeout('HidenProps()', 5000);
                }
            });
        }
    });
}

function SendTicket(){
    $('#btn-ticket').unbind('click');
    $('#btn-ticket').click(function(){
        if(aProd.length>0){
            var form = $('#form-ticket');
            if(form.validationEngine('validate')){
                var parametros = {
                    'form': form.serialize(),
                    'aProd': aProd
                };
                $.ajax({
                    data: parametros,
                    url: 'saveticket',
                    type: 'POST',
                    success: function(result){
                        if(result.response){
                            SuccessMessage(result.message);
                            location.reload();
                        }else{
                            ErrorMessage(result.message);
                        }
                    }
                });
            }
        }else{
            ErrorMessage('DEBE INGRESAR PRODUCTOS PARA REGISTRAR EL TICKET');
        }
    });
}

function findIn2dArray(arr_2d, val){
    var indexArr = $.map(arr_2d, function(arr, i){
        if($.inArray(val, arr) != -1) {
            return 1;
        }
        return -1;
    });
    return indexArr.indexOf(1);
}

function CalculateTotal(){
    var total=0;
    $.each(ventas.column(5).data(), function(index, value){
        var span = $.parseHTML(value)[1];
        var id = $(span).attr('id');
        var monto = $('#'+id).text();
        total += parseInt(monto);
    });
    $('#total-price').text('');
    $('#total-price').text(total);
}