$(document).ready(function(){ 
    ValidateUser();
});

$(document).keypress(function(e){
    if(e.which == 13) {
        SendForm();
    }
});

function ValidateUser(){
    $('.button-login').click(function(){
        SendForm();
    });
}

function SendForm(){
    $('.btn-login').prop('disabled', true);
    var form = $('.form-login');
    if(form.validationEngine('validate')){
        $.ajax({
            data: form.serialize(),
            url: 'access',
            type: 'POST',
            success: function(result)
            {
                var msn = result;
                if(msn['response']){
                    //redirect
                    $(location).attr('href',msn['message']);
                    $('.btn-login').prop('disabled', true);
                }else{
                    ErrorMessage(msn['message']);
                    $('.btn-login').prop('disabled', false);
                }
            }
        });
    }
}