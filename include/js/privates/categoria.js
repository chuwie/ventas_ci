var table;
$(document).ready(function(){
    Savecategory();
    LoadTableCategory();
});

function Savecategory(){
    $('#btn-save').click(function(){
        var form = $('#form-category');
        if(form.validationEngine('validate'))
        {
            $.ajax({
                data: form.serialize(),
                url: 'savecategory',
                type: 'POST',
                success: function(result)
                {
                    if(result.response == 1){
                        SuccessMessage(result.message);
                        LoadCategory();
                        Clean();
                        table.ajax.reload();
                    }else{
                        ErrorMessage(result.message);
                    }
                }
            });
        }
    });
}

function Clean(){
    $('#name').val('');
    $('#category').val(0);
    $('#select2-category-container').text('Categoría Padre');
    $('#btn-save').text('CREAR');
}

function LoadCategory(){
    $.ajax({
        url: 'getcategory',
        type: 'POST',
        success: function(result){
            $('#category').html('');
            $('#category').append(result);
        }
    });
}

function LoadTableCategory(){
    table = $("#tcategory").DataTable({
        "language":{
            "decimal":        "",
            "emptyTable":     "No hay datos",
            "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
            "infoFiltered":   "(Filtro de _MAX_ total registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "zeroRecords":    "No se encontraron coincidencias",
            "paginate": {
                "first":      "Primero",
                "last":       "Ultimo",
                "next":       "Próximo",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar orden de columna ascendente",
                "sortDescending": ": Activar orden de columna desendente"
            }
        },
        "searching": true,
        "serverSide": false,
        "ordering": false,
        "bLengthChange": false,
        "bPaginate": false,
        "info": false,
        "scrollY": "500px",
        "scrollCollapse": true,
        "destroy": true,
        "fixedColumns": true,
        "scrollX": true,
        "ajax": 
        {
            url: "loadcategory",
            type: "POST",
            dataSrc: "",
            error: function (error) {
                ErrorMessage(error);
            }

        },
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "modify" }
        ],
        "drawCallback": function( settings ) {
            LoadCategory();
            // $(".loader").fadeOut("slow");
        }
    });
}

function ModifyCategory(category, name, father, e){
    e.preventDefault();
    $("html, body").animate({scrollTop:"0px"});

    $('#id_category').val(category);
    $('#name').val(name);
    if(father==0)
        father='';
    $('#category').val(father);
    $textcategory = $('#category :selected').text();
    $('#select2-category-container').text($textcategory);

    $('#btn-save').text('ACTUALIZAR');
}







