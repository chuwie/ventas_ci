var table;
$(document).ready(function(){
    Savebrands();
    LoadTableBrands();
});

function Savebrands(){
    $('#btn-save').click(function(){
        var form = $('#form-brands');
        if(form.validationEngine('validate'))
        {
            $.ajax({
                data: form.serialize(),
                url: 'savebrand',
                type: 'POST',
                success: function(result)
                {
                    if(result.response == 1){
                        SuccessMessage(result.message);
                        Clean();
                        table.ajax.reload();
                    }else{
                        ErrorMessage(result.message);
                    }
                }
            });
        }
    });
}

function Clean(){
    $('#name').val('');
    $('#btn-save').text('CREAR');
}

function LoadTableBrands(){
    table = $("#tbrands").DataTable({
        "language":{
            "decimal":        "",
            "emptyTable":     "No hay datos",
            "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
            "infoFiltered":   "(Filtro de _MAX_ total registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "zeroRecords":    "No se encontraron coincidencias",
            "paginate": {
                "first":      "Primero",
                "last":       "Ultimo",
                "next":       "Próximo",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar orden de columna ascendente",
                "sortDescending": ": Activar orden de columna desendente"
            }
        },
        "searching": true,
        "serverSide": true,
        "ordering": false,
        "bLengthChange": true,
        "bPaginate": true,
        "destroy": true,
        "fixedColumns": true,
        "scrollX": true,
        "ajax": 
        {
            url: "getbrands",
            type: "POST",
            error: function (error) {
                ErrorMessage(error);
            }
        },
        "columns": [
            { "data": "name" },
            { "data": "modify" },
            { "data": "state" }
        ],
        "drawCallback": function( settings ){
            // $(".loader").fadeOut("slow");
        }
    });
}

function ModifyBrands(brands, name, e){
    e.preventDefault();
    $("html, body").animate({scrollTop:"0px"});

    $('#id_brands').val(brands);
    $('#name').val(name);

    $('#btn-save').text('ACTUALIZAR');
}

function ChangeState(toggle, brands)
{
    var state = toggle.prop('checked');
    var parametros = {
        brands: brands,
        state: state
    };
    $.ajax({
        data: parametros, 
        url: 'changestatebrands', 
        type: 'POST', 
        success: function(result){
            if(result.response == 1){
                SuccessMessage(result.message);
            }
        }
    });
}





