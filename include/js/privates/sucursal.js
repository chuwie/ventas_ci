var table;

$(document).ready(function(){
    LoadBranchofficeTable();
});

function SaveBrandoffice(){
    var form = $('#form-branchoffice');
    if(form.validationEngine('validate')){
        $.ajax({
            data: form.serialize(),
            url: 'savebranchoffice',
            type: 'POST',
            success: function(result){
                if(result.response == 1){
                    SuccessMessage(result.message);
                    Clean();
                    table.ajax.reload();
                }else{
                    ErrorMessage(result.message);
                }
            }
        });
    }
}

function Clean(){
    $('#direction').val('');
    $('#btn-save').text('CREAR');
}

function LoadBranchofficeTable(){
    table = $("#tbranchoffice").DataTable({
        "language":{
            "decimal":        "",
            "emptyTable":     "No hay datos",
            "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
            "infoFiltered":   "(Filtro de _MAX_ total registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "zeroRecords":    "No se encontraron coincidencias",
            "paginate": {
                "first":      "Primero",
                "last":       "Ultimo",
                "next":       "Próximo",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar orden de columna ascendente",
                "sortDescending": ": Activar orden de columna desendente"
            }
        },
        "searching": true,
        "serverSide": true,
        "ordering": false,
        "bLengthChange": true,
        "bPaginate": true,
        "destroy": true,
        "fixedColumns": true,
        "scrollX": true,
        "ajax": {
            url: "loadbranchoffice",
            type: "POST",
            error: function (error) {
                alert(error);
            }
        },
        "columns": [
            { "data": "branchoffice" },
            { "data": "modify" },
            { "data": "state" }
        ],
        "drawCallback": function( settings ) {
            $(".loader").fadeOut("slow");
        }
    });
}

function ModifyBranchoffice(branchoffice, direction, e){
    e.preventDefault();
    $("html, body").animate({scrollTop:"0px"});

    $('#id_branchoffice').val(branchoffice);
    $('#direction').val(direction);
    $('#btn-save').text('ACTUALIZAR');
}

function ChangeState(toggle, branchoffice){
    var state = toggle.prop('checked');
    var parametros = {
        branchoffice: branchoffice,
        state: state
    };
    $.ajax({
        data:  parametros, 
        url:   'changestatebranchoffice', 
        type:  'POST', 
        success: function(result){
            if(result.response == 1){
                SuccessMessage(result.message);
            }
        }
    });
}