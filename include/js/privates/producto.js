var table;
$(document).ready(function(){
    LoadProductTable();
});

$('#precio').on('keypress', function(){
    return soloNumeros(event);
});

$('#name').on('keypress', function(){
    return soloNumerosLetras(event);
});

function SaveProduct(){
    var form = $('#form-product');
    if(form.validationEngine('validate')){
        $.ajax({
            data: form.serialize(),
            url: 'saveproduct',
            type: 'POST',
            success: function(result){
                if(result.response == 1){
                    SuccessMessage(result.message);
                    Clean();
                    table.ajax.reload();
                }else{
                    ErrorMessage(result.message);
                }
            }
        });
    }
}

function Clean(){
    $('#id_product').val('');
    $('#name').val('');
    $('#precio').val(1);
    $('#detail').val('');
    $('#date-mask').val('');
    $('#brand').val('');
    $('#select2-brand-container').text($('#brand :selected').text());
    $('#category').val('');
    $('#select2-category-container').text($('#category :selected').text());
    $('#btn-save').text('CREAR');
}

function LoadProductTable(){
    table = $("#tproduct").DataTable({
        "language":{
            "decimal":        "",
            "emptyTable":     "No hay datos",
            "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
            "infoFiltered":   "(Filtro de _MAX_ total registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "zeroRecords":    "No se encontraron coincidencias",
            "paginate": {
                "first":      "Primero",
                "last":       "Ultimo",
                "next":       "Próximo",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar orden de columna ascendente",
                "sortDescending": ": Activar orden de columna desendente"
            }
        },
        "searching": true,
        "serverSide": true,
        "ordering": false,
        "bLengthChange": true,
        "bPaginate": true,
        "destroy": true,
        "fixedColumns": true,
        "scrollX": true,
        "ajax": {
            url: "loadproduct",
            type: "POST",
            error: function (error) {
                ErrorMessage(error);
            }
        },
        "columns": [
            { "data": "name" },
            { "data": "price" },
            { "data": "brand" },
            { "data": "category" },
            { "data": "date" },
            { "data": "code" },
            { "data": "bar" },
            { "data": "modify" },
            { "data": "state" }
        ],
        "drawCallback": function(settings){
            $(".loader").fadeOut("slow");
        }
    });
}

function ModifyProducto(product, name, price, expiration, detail, category, brand, e){
    e.preventDefault();
    $("html, body").animate({scrollTop:"0px"});

    $('#id_product').val(product);
    $('#name').val(name);
    $('#precio').val(price);
    if(expiration!='0000-00-00')
        $('#date-mask').val(expiration);
    $(detail!='')
        $('#detail').val(detail);

    $('#brand').val(brand);
    $('#select2-brand-container').text($('#brand :selected').text());
    $('#category').val(category);
    $('#select2-category-container').text($('#category :selected').text());

    $('#btn-save').text('ACTUALIZAR');
}

function ChangeState(toggle, product){
    var state = toggle.prop('checked');
    var parametros = {
        product: product,
        state: state
    };
    $.ajax({
        data: parametros, 
        url: 'changestateproduct', 
        type: 'POST', 
        success: function(result){
            if(result.response == 1){
                SuccessMessage(result.message);
            }
        }
    });
}

function DownloadBarcode(codigo, e){
    e.preventDefault();
    $.ajax({
        data: {'codigo': codigo},
        url: 'barcode',
        type: 'POST',
        success: function(result){
            // console.log(result);
            eval(result);
        }
    });
    
}