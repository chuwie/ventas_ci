$(document).ready(function(){
    UpdateData();
    $('#auto').click(function(){
        AutoPassword();
    });
});

function UpdateData(){
    $('#btn-save').unbind('click');
    $('#btn-save').click(function(){
        var form = $('#form-updateuser');
        if(form.validationEngine('validate')){
            $.ajax({
                data: form.serialize(),
                url: 'updatedata',
                type: 'POST',
                success: function(result){
                    if(result.response == 1){
                        SuccessMessage(result.message);
                        LoadCategory();
                        Clean();
                        table.ajax.reload();
                    }else{
                        ErrorMessage(result.message);
                    }
                }
            });
        }
    });
}

function AutoPassword(){
    var chars = "0123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var pass = "";
    for(var x = 0; x < 24; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    $('#new').val(pass);
}