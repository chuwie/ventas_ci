var table;

$(document).ready(function(){
    LoadStockProductTable();
});

$('#stock').on('keypress', function(){
    return soloNumeros(event);
});

function SaveStock(){
    var form = $('#form-stock');
    $('#product').prop('disabled', false);
    $('#branchoffice').prop('disabled', false);
    if(form.validationEngine('validate')){
        var send = form.serialize();
        $('#product').prop('disabled', true);
        $('#branchoffice').prop('disabled', true);
        $.ajax({
            data: send,
            url: 'savestock',
            type: 'POST',
            success: function(result){
                if(result.response == 1){
                    SuccessMessage(result.message);
                    Clean();
                    table.ajax.reload();
                }else{
                    ErrorMessage(result.message);
                }
            }
        });
    }
}

function Clean(){
    $('#product').val(0);
    $('#stock').val(1);
    $('#branchoffice').val(0);
    $('#select2-product-container').text('Búscar Producto');
    $('#select2-branchoffice-container').text('Búscar Sucursal');
    $('#btn-save').text('CREAR');
    $('#product').prop('disabled', false);
    $('#branchoffice').prop('disabled', false);
    $('#btn-save').prop('disabled', false);
    $('#btn-save').unbind('change');
}

function LoadStockProductTable(){
    table = $("#tstock").DataTable({
        "language":{
            "decimal":        "",
            "emptyTable":     "No hay datos",
            "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
            "infoFiltered":   "(Filtro de _MAX_ total registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "zeroRecords":    "No se encontraron coincidencias",
            "paginate": {
                "first":      "Primero",
                "last":       "Ultimo",
                "next":       "Próximo",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Activar orden de columna ascendente",
                "sortDescending": ": Activar orden de columna desendente"
            }
        },
        "searching": true,
        "serverSide": true,
        "ordering": false,
        "bLengthChange": true,
        "bPaginate": true,
        "destroy": true,
        "fixedColumns": true,
        "scrollX": true,
        "ajax": {
            url: "loadstockproduct",
            type: "POST",
            error: function(error){
                alert(error);
            }
        },
        "columns": [
            { "data": "name" },
            { "data": "direction" },
            { "data": "stock" },
            { "data": "modify" }
        ],
        "drawCallback": function( settings ) {
            $(".loader").fadeOut("slow");
        }
    });
}

function ModifyStockProducto(id, product, branchoffice, stock, e){
    e.preventDefault();
    $("html, body").animate({scrollTop:"0px"});

    $('#product').val(product);
    $texprod = $('#product :selected').text();
    $('#select2-product-container').text($texprod);
    $('#product').prop('disabled', true);

    $('#branchoffice').val(branchoffice);
    $textbranch = $('#branchoffice :selected').text();
    $('#select2-branchoffice-container').text($textbranch);
    $('#branchoffice').prop('disabled', true);

    $('#stock').val(stock);
    $('#id').val(id);

    $('#btn-save').text('ACTUALIZAR');
    $('#btn-save').prop('disabled', true);

    ChangeStock(stock);
}

function ChangeStock(stock){
    $('#stock').change(function(){
        if($(this).val()==stock){
            $('#btn-save').prop('disabled', true);
        }else{
            $('#btn-save').prop('disabled', false);
        }
    });
}
